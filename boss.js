const robot = require('robotjs');
const { tintureChallengeBoss, generateNitro, goChallenge, useDropBoost, craftAlchemyItem } = require("./utils");

const isDifficultBoss = process.argv.slice(2).indexOf('--difficult') !== -1;
// farm golem by default
const challengeNumber = parseInt(process.argv.slice(2)[0], 10) || 2;
let runCount = 0;

async function farmChallenge() {
  runCount++;
  await goChallenge(challengeNumber);
  await tintureChallengeBoss(challengeNumber, isDifficultBoss ? 1 : 3);
  if (runCount % (isDifficultBoss ? 5 * 3 : 3) === 1) {
    await generateNitro();
  }
  if (runCount % (isDifficultBoss ? 5 * 5 : 5) === 1) {
    await useDropBoost();
  }
  if (isDifficultBoss && (runCount % 3) === 1) {
    // craft 10 strength capsules
    await craftAlchemyItem(3, 12, 3);
  }
  robot.keyTap('u');
}


farmChallenge();
setInterval(farmChallenge, isDifficultBoss ? 1000 * 5 : 1000 * 25);
