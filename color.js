// @ts-check

const robot = require('robotjs');

exports.getCurrentMousePosPixelColor = getCurrentMousePosPixelColor;
exports.getColorValue = getColorValue;
exports.getPositionColorValue = getPositionColorValue;

function getCurrentMousePosPixelColor() {
  const pos = robot.getMousePos();
  return robot.getPixelColor(pos.x, pos.y);
}

/**
 * @param {string} color
 */
function getColorValue(color) {
  if (color.length !== 6) {
    return NaN;
  }
  return parseInt(color.slice(0, 2), 16)
    + parseInt(color.slice(2, 4), 16)
    + parseInt(color.slice(4, 6), 16);
}

/**
 * @param {[number, number]} pos
 */
function getPositionColorValue(pos) {
  return getColorValue(robot.getPixelColor(...pos));
}
