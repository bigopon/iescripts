// @ts-check
/// <reference path="./types.d.ts" />

/**@type {{ [T in PlayerClass]: number }} */
exports.classNumberMap = {
  War: 1,
  War_R: 2,
  Wiz: 3,
  Wiz_R: 4,
  Ang: 5,
  Ang_R: 6,
};
