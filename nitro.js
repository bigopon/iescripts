const { generateNitro, useDropBoost, useSlimeCoinBoost } = require('./utils');

let runNumber = 0;

async function run() {
  runNumber++;
  await generateNitro();
  if (runNumber % 5 === 1) {
    await useDropBoost();
    await useSlimeCoinBoost();
  }
}

run();
setInterval(() => run(), 1000 * 85);
