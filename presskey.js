var robot = require('robotjs');

const now = Date.now();

robot.mouseClick();

robot.keyToggle('w', 'down');

const TEN_SECONDS = 1000 * 10;

while (true) {
  const $now = Date.now();
  if (($now - now) >= TEN_SECONDS) {
    process.exit(0);
  }
}
