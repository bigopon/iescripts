// @ts-check
const robot = require('robotjs');
const screenSize = robot.getScreenSize();
const {
  positions: $positions,
  basePositions: $basePositions,
  capturePositions: $capturePositions,
} = require(`./positions_${screenSize.width}_${screenSize.height}`);

exports.positions = {
  /**@type {[number, number]}
   // @ts-expect-error */
  Class_Selection_War:  [730, 490],
  /**@type {[number, number]}
   // @ts-expect-error */
  Class_Selection_Wiz:  [800, 490],
  /**@type {[number, number]}
   // @ts-expect-error */
  Class_Selection_Ang:  [870, 490],
  /**@type {[number, number]}
   // @ts-expect-error */
  Class_Selection_Confirm_Btn: [800, 753],
  /**@type {[number, number]}
   // @ts-expect-error */
  Click_Stone:          [450, 675],
  /**@type {[number, number]}
   // @ts-expect-error */
  Click_Crystal:        [575, 675],
  /**@type {[number, number]}
   // @ts-expect-error */
  Click_Leaf:           [690, 675],
  /**@type {[number, number]}
   // @ts-expect-error */
  Upgrade_Slime_Bank:   [610, 930],
  /**@type {[number, number]}
   // @ts-expect-error */
  CraftPanel:           [446, 990],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Items_Tab:      [430, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_Tab:    [520, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_UseAll: [701, 864],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Inventory_Tab:  [630, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_D:              [634, 606],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_C:              [661, 606],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_B:              [688, 606],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_A:              [677, 608],
  /**@type {[number, number]}
   // @ts-expect-error */
  Nitro_Shortcut:       [882, 882],
  /**@type {[number, number]}
   // @ts-expect-error */
  Auto_Move:            [824, 887],
  /**@type {[number, number]}
   // @ts-expect-error */
  Start_Challenge:      [495, 925],
  /**@type {[number, number]}
   // @ts-expect-error */
  Quit_Challenge:       [688, 917],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_War:          [510, 605],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Wiz:          [510, 630],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Ang:          [510, 660],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Btn:          [530, 945],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Confirm_Btn:  [805, 875],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rein_To_Rebirth_Btn:  [565, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_To_Rein_Btn:  [515, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rein_Btn:             [575, 940],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rein_Confirm_Btn:     [805, 875],
  /**@type {[number, number]}
   // @ts-expect-error */
  BlackFairy_44:        [975, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  NineTailFox_54:       [975, 635],
};

exports.delays = {
  MoveMouse: 66,
  KeyNormal: 16,
  ClickPanel: 501, // changing panel could take up to half a second
  ClickNormal: 66, // clicking on anything should be delayed a bit
};

const basePositions = exports.basePositions = {

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_0_0:             [400, 400],

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_max_max:         [1200, 1000],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtn:               [470, 970],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtnJump:           [90,  20],

  /**@type {[number, number]}
   // @ts-expect-error */
  resourceUpgrade:        [435, 775],

  /**@type {[number, number]}
   // @ts-expect-error */
  resourceUpgradeJump:    [40,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgrade:       [435, 820],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgradeJump:   [40,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanel:        [500, 875],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanelJump:    [150, 30],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkill:             [450, 615],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillJump:         [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillLevelup:      [563, 628],

  /**@type {[number, number]}
    // @ts-expect-error */
  classSkillLevelupJump:  [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStance:       [560, 615],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStanceJump:   [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  skillSet:               [807, 918],

  /**@type {[number, number]}
   // @ts-expect-error */
  skillSetJump:           [0,   5],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStage:              [445, 695],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStageJump:          [55,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeon:                [445, 845],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeonJump:            [50,  60],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipment:         [455, 650],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipmentJump:     [60,  60],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtn:           [445, 605],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtnJump:       [55,  20],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtn:          [435, 605],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtnJump:      [50,  10],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItem:            [435, 695],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItemJump:        [45,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItem:     [445, 895],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItemJump: [37,  37],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyStat:            [465, 832],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyStatJump:        [0,   10],

  /**@type {[number, number]}
   // @ts-expect-error */
  inventorySellBtn:       [676, 623],

  /**@type {[number, number]}
   // @ts-expect-error */
  inventorySellBtnJump:   [0,   18],

  /**@type {[number, number]}
   // @ts-expect-error */
  inventorySellMaxBtn:    [708, 623],

  /**@type {[number, number]}
   // @ts-expect-error */
  inventorySellMaxBtnJump:[0,   18],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBoss:          [445, 625],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBossJump:      [52,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  questClaim:             [467, 641],

  /**@type {[number, number]}
   // @ts-expect-error */
  questClaimJump:         [50,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  dailyQuestClaim:        [465, 900],

  /**@type {[number, number]}
   // @ts-expect-error */
  dailyQuestClaimJump:    [50,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  rebirthUpgrade:         [450, 825],

  /**@type {[number, number]}
   // @ts-expect-error */
  rebirthUpgradeJump:     [50,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlot:        [1035, 930],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlotJump:    [30,  30],

  /**@type {[number, number]}
   // @ts-expect-error */
  systemConfigCheckbox:   [430, 630],

  /**@type {[number, number]}
   // @ts-expect-error */
  systemConfigCheckboxJump: [0,  20],

  /**@type {[number, number]}
   // @ts-expect-error */
  mob:                    [795, 485],

  /**@type {[number, number]}
   // @ts-expect-error */
  mobJump:                [40,  35],
};

/**@type {Record<number, [number, number][]>} */
exports.capturePositions = {};

// update position based on different comp screen resolutions
Object.assign(exports.positions, $positions);
Object.assign(exports.basePositions, $basePositions);
Object.assign(exports.capturePositions, $capturePositions);

exports.getResourceUpgradeItemPosition = getResourceUpgradeItemPosition;
exports.getSlimeBankUpgradeItemPosition = getSlimeBankUpgradeItemPosition;
exports.getCraftEquipmentPosition = getCraftEquipmentPosition;
exports.getPanelBtnPosition = getPanelBtnPosition;
exports.getEnterAreaBtnPosition = getEnterAreaBtnPosition;
exports.getAreaStagePosition = getAreaStagePosition;
exports.getDungeonPosition = getDungeonPosition;
exports.getChallengePosition = getChallengePosition;
exports.getAlchemyTabBtnPosition = getAlchemyTabBtnPosition;
exports.getAlchemyItemPosition = getAlchemyItemPosition;
exports.getAlchemyCraftedItemPosition = getAlchemyCraftedItemPosition;
exports.getAlchemyStatBtnPosition = getAlchemyStatBtnPosition;
exports.getInventoryItemSellMaxBtnPosition = getInventoryItemSellMaxBtnPosition;
exports.getMobPosition = getMobPosition;
exports.getClassSkillPanelPosition = getClassSkillPanelPosition;
exports.getClassSkillPosition = getClassSkillPosition;
exports.getClassSkillLevelupPosition = getClassSkillLevelupPosition;
exports.getClassSkillStancePosition = getClassSkillStancePosition;
exports.getGlobalSkillSlotPosition = getGlobalSkillSlotPosition;
exports.getSkillSetBtnPosition = getSkillSetBtnPosition;
exports.getOneTimeQuestRewardClaimBtnPosition = getOneTimeQuestRewardClaimBtnPosition;
exports.getDailyQuestRewardClaimBtnPosition = getDailyQuestRewardClaimBtnPosition;
exports.getRebirthUpgradePosition = getRebirthUpgradePosition;
exports.getSystemConfigCheckboxPosition = getSystemConfigCheckboxPosition;

/**
 * @param {number} panelNumber
 * @returns {[number, number]}
 */
function getPanelBtnPosition(panelNumber) {
  const [baseX, baseY] = basePositions.panelBtn;
  if (panelNumber < 1 || panelNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.panelBtnJump;
  return [
    baseX + ((panelNumber - 1) % 4) * jumpX,
    baseY + (panelNumber > 4 ? jumpY : 0)
  ];
}

/**
 * Get item screen position based on their position number
 * @param {number} upgradeNumber left->right |> top->bottom order, from 1 - 32
 * @returns {[number, number]}
 */
function getResourceUpgradeItemPosition(upgradeNumber) {
  const [baseX, baseY] = basePositions.resourceUpgrade;
  if (upgradeNumber < 1 || upgradeNumber > 32) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.resourceUpgradeJump;
  return [
    baseX + ((upgradeNumber - 1) % 8) * jumpX,
    baseY + Math.floor(Math.max(upgradeNumber - 1, 0) / 8) * jumpY
  ];
}

/**
 * Get item screen position based on their position number
 * @param {number} upgradeNumber left->right |> top->bottom order, from 1 - 32
 * @returns {[number, number]}
 */
function getSlimeBankUpgradeItemPosition(upgradeNumber) {
  const [baseX, baseY] = basePositions.slimeBankUpgrade;
  if (upgradeNumber < 1 || upgradeNumber > 32) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.slimeBankUpgradeJump;
  return [
    baseX + ((upgradeNumber - 1) % 8) * jumpX,
    baseY + Math.floor(Math.max(upgradeNumber - 1, 0) / 8) * jumpY
  ];
}

/**
 * Get item screen position based on their position number
 * @param {number} itemNumber left top right top to bottom order, from 1 - 10
 * @returns {[number, number]}
 */
function getCraftEquipmentPosition(itemNumber) {
  const [baseX, baseY] = basePositions.craftEquipment;
  if (itemNumber < 1 || itemNumber > 10) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.craftEquipmentJump;
  return [
    baseX + ((itemNumber - 1) % 5) * jumpX,
    baseY + (itemNumber > 5 ? jumpY : 0)
  ];
}

/**
 * Get area screen positions based on their position number, each area has 8 stages
 * @param {number} areaNumber left top right top to bottom order, from 1 - 10
 * @returns {[number, number]}
 */
function getEnterAreaBtnPosition(areaNumber) {
  const [baseX, baseY] = basePositions.enterAreaBtn;
  if (areaNumber < 1 || areaNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.enterAreaBtnJump;
  return [
    baseX + ((areaNumber - 1) % 4) * jumpX,
    baseY + (areaNumber > 4 ? jumpY : 0)
  ];
}

/**
 * Get alchemy tab button positions based on their position number
 * @param {number} tabNumber left -> right | top -> bottom order, from 1 - 14
 * @returns {[number, number]}
 */
function getAlchemyTabBtnPosition(tabNumber) {
  const [baseX, baseY] = basePositions.alchemyTabBtn;
  if (tabNumber < 1 || tabNumber > 14) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.alchemyTabBtnJump;
  return [
    baseX + ((tabNumber - 1) % 7) * jumpX,
    baseY + (tabNumber > 7 ? jumpY : 0)
  ];
}

/**
 * Get alchemy item button positions based on their position number
 * @param {number} itemNumber left -> right | top -> bottom order, from 1 - 21
 * @returns {[number, number]}
 */
function getAlchemyItemPosition(itemNumber) {
  const [baseX, baseY] = basePositions.alchemyItem
  if (itemNumber < 1 || itemNumber > 21) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.alchemyItemJump;
  return [
    baseX + ((itemNumber - 1) % 7) * jumpX,
    baseY + Math.floor(Math.max(itemNumber - 1, 0) / 7) * jumpY
  ];
}

/**
 * Get alchemy item button positions based on their position number
 * @param {number} itemNumber left -> right | top -> bottom order, from 1 - 14
 * @returns {[number, number]}
 */
function getAlchemyCraftedItemPosition(itemNumber) {
  const [baseX, baseY] = basePositions.alchemyCraftedItem;
  if (itemNumber < 1 || itemNumber > 16) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.alchemyCraftedItemJump;
  return [
    baseX + ((itemNumber - 1) % 7) * jumpX,
    baseY + Math.floor(Math.max(itemNumber - 1, 0) / 10) * jumpY
  ];
}

/**
 * @param {number} statNumber
 * @returns {[number, number]}
 */
function getAlchemyStatBtnPosition(statNumber) {
  const [baseX, baseY] = basePositions.alchemyStat;
  if (statNumber < 1 || statNumber > 4) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.alchemyStatJump;
  return [
    baseX + ((statNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(statNumber - 1, 0) / 1) * jumpY
  ];
}

/**
 * @param {number} itemNumber
 * @returns {[number, number]}
 */
function getInventoryItemSellMaxBtnPosition(itemNumber) {
  const [baseX, baseY] = basePositions.inventorySellMaxBtn;
  if (itemNumber < 1 || itemNumber > 15) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.inventorySellMaxBtnJump;
  return [
    baseX + ((itemNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(itemNumber - 1, 0) / 1) * jumpY
  ];
}

/**
 * Get area screen position based on their position number
 * @param {number} chanllengeNumber left -> right | top -> bottom order, from 1 - 8
 * @returns {[number, number]}
 */
function getChallengePosition(chanllengeNumber) {
  const [baseX, baseY] = basePositions.challengeBoss;
  if (chanllengeNumber < 1 || chanllengeNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.challengeBossJump;
  return [
    baseX + ((chanllengeNumber - 1) % 6) * jumpX,
    baseY + (chanllengeNumber > 6 ? jumpY : 0)
  ];
}

/**
 * Get area screen position based on their position number
 * @param {number} areaNumber left -> right | top -> bottom order, from 1 - 8
 * @returns {[number, number]}
 */
function getAreaStagePosition(areaNumber) {
  const [baseX, baseY] = basePositions.areaStage;
  if (areaNumber < 1 || areaNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.areaStageJump;
  return [
    baseX + ((areaNumber - 1) % 4) * jumpX,
    baseY + (areaNumber > 4 ? jumpY : 0)
  ];
}

/**
 * Get the position of a dungeon based on its position number
 * @param {number} dungeonNumber left -> right | top -> bottom order, from 1 - 8
 * @returns {[number, number]}
 */
function getDungeonPosition(dungeonNumber) {
  const [baseX, baseY] = basePositions.dungeon;
  if (dungeonNumber < 1 || dungeonNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.dungeonJump;
  return [
    baseX + ((dungeonNumber - 1) % 6) * jumpX,
    baseY + (dungeonNumber > 6 ? jumpY : 0)
  ];
}

/**
 * @param {number} positionNumber the position of a mob. Number left->right & top->bottom. top left is 1.
 * @returns {[number, number]}
 */
function getMobPosition(positionNumber) {
  const [baseX, baseY] = basePositions.mob;
  if (positionNumber < 1 || positionNumber > 100) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.mobJump;
  return [
    baseX + ((positionNumber - 1) % 10) * jumpX,
    baseY + Math.floor(Math.max(positionNumber - 1, 0) / 10) * jumpY
  ];
}

/**
 * @param {number} klass the position of a class skill panel. Left->right & Top->Bottom order from 1->6
 * @returns {[number, number]}
 */
function getClassSkillPanelPosition(klass) {
  const [baseX, baseY] = basePositions.classSkillPanel;
  if (klass < 1 || klass > 6) {
    throw new Error('Invalid class number: ' + klass);
  }
  const [jumpX, jumpY] = basePositions.classSkillPanelJump;
  return [
    baseX + ((klass - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(klass - 1, 0) / 2) * jumpY
  ];
}

/**
 * @param {number} skillNumber the position of a skill. Left->right & Top->Bottom order from 1->10
 * @returns {[number, number]}
 */
function getClassSkillPosition(skillNumber) {
  const [baseX, baseY] = basePositions.classSkill;
  if (skillNumber < 1 || skillNumber > 16) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.classSkillJump;
  return [
    baseX + ((skillNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(skillNumber - 1, 0) / 2) * jumpY
  ];
}

/**
 * @param {number} skillNumber the position of a skill. Left->right & Top->Bottom order from 1->10
 * @returns {[number, number]}
 */
function getClassSkillLevelupPosition(skillNumber) {
  const [baseX, baseY] = basePositions.classSkillLevelup;
  if (skillNumber < 1 || skillNumber > 16) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.classSkillLevelupJump;
  return [
    baseX + ((skillNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(skillNumber - 1, 0) / 2) * jumpY
  ];
}

/**
 * @param {number} stanceNumber the position of a skill stance. Left->right & Top->Bottom order from 1->10
 * @returns {[number, number]}
 */
function getClassSkillStancePosition(stanceNumber) {
  const [baseX, baseY] = basePositions.classSkillStance;
  if (stanceNumber < 1 || stanceNumber > 16) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.classSkillStanceJump;
  return [
    baseX + ((stanceNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(stanceNumber - 1, 0) / 2) * jumpY
  ];
}

/**
 * @param {number} skillNumber the position of a skill stance. Left->right & Top->Bottom order from 1->10
 * @returns {[number, number]}
 */
function getGlobalSkillSlotPosition(skillNumber) {
  const [baseX, baseY] = basePositions.globalSkillSlot;
  if (skillNumber < 1 || skillNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.globalSkillSlotJump;
  return [
    baseX + ((skillNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(skillNumber - 1, 0) / 2) * jumpY
  ];
}

/**
 * @param {number} skillSetNumber the position of a skill stance. Left->right & Top->Bottom order from 1->10
 * @returns {[number, number]}
 */
function getSkillSetBtnPosition(skillSetNumber) {
  const [baseX, baseY] = basePositions.skillSet;
  if (skillSetNumber < 1 || skillSetNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.skillSetJump;
  return [
    baseX + ((skillSetNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(skillSetNumber - 1, 0) / 1) * jumpY
  ];
}

/**
 * @param {number} questNumber the position of a quest. Left->right & Top->Bottom order from 1->12
 * @returns {[number, number]}
 */
function getOneTimeQuestRewardClaimBtnPosition(questNumber) {
  const [baseX, baseY] = basePositions.questClaim;
  if (questNumber < 1 || questNumber > 9) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.questClaimJump;
  return [
    baseX + ((questNumber - 1) % 6) * jumpX,
    baseY + Math.floor(Math.max(questNumber - 1, 0) / 6) * jumpY
  ];
}

/**
 * @param {number} questNumber the position of a quest. Left->right & Top->Bottom order from 1->12
 * @returns {[number, number]}
 */
function getRepeatableQuestRewardClaimBtnPosition(questNumber) {
  const [baseX, baseY] = basePositions.questClaim;
  if (questNumber < 1 || questNumber > 8) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.questClaimJump;
  return [
    baseX + ((questNumber - 1) % 6) * jumpX,
    baseY + Math.floor(Math.max(questNumber - 1, 0) / 6) * jumpY
  ];
}

/**
 * @param {number} questNumber the position of a quest. Left->right & Top->Bottom order from 1->12
 * @returns {[number, number]}
 */
function getDailyQuestRewardClaimBtnPosition(questNumber) {
  const [baseX, baseY] = basePositions.dailyQuestClaim;
  if (questNumber < 1 || questNumber > 4) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.dailyQuestClaimJump;
  return [
    baseX + ((questNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(questNumber - 1, 0) / 2) * jumpY
  ];
}

/**
 * Get the position of a dungeon based on its position number
 * @param {number} upgradeNumber left -> right | top -> bottom order, from 1 - 8
 * @returns {[number, number]}
 */
function getRebirthUpgradePosition(upgradeNumber) {
  const [baseX, baseY] = basePositions.rebirthUpgrade;
  if (upgradeNumber < 1 || upgradeNumber > 18) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.rebirthUpgradeJump;
  return [
    baseX + ((upgradeNumber - 1) % 6) * jumpX,
    baseY + Math.floor(Math.max(upgradeNumber - 1, 0) / 6) * jumpY
  ];
}

/**
 * @param {number} configNumber
 * @returns {[number, number]}
 */
function getSystemConfigCheckboxPosition(configNumber) {
  const [baseX, baseY] = basePositions.systemConfigCheckbox;
  if (configNumber < 1 || configNumber > 14) {
    return [baseX, baseY];
  }
  const [jumpX, jumpY] = basePositions.systemConfigCheckboxJump;
  return [
    baseX + ((configNumber - 1) % 2) * jumpX,
    baseY + Math.floor(Math.max(configNumber - 1, 0) / 1) * jumpY
  ];
}
