// @ts-check

exports.positions = {
  /**@type {[number, number]}
   // @ts-expect-error */
  Upgrade_Slime_Bank:   [433, 647],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Items_Tab:      [315, 400],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_Tab:    [375, 400],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_UseAll: [500, 600],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Inventory_Tab:  [450, 400],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_D:              [453, 422],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_C:              [471, 422],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_B:              [490, 422],
  /**@type {[number, number]}
   // @ts-expect-error */
  Nitro_Shortcut:       [624, 614],
  /**@type {[number, number]}
   // @ts-expect-error */
  Auto_Move:            [585, 615],
  /**@type {[number, number]}
   // @ts-expect-error */
  Start_Challenge:      [375, 640],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_War:          [350, 420],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Wiz:          [350, 438],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Ang:          [350, 456],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Btn:          [385, 658],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Confirm_Btn:  [567, 608],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rein_To_Rebirth_Btn:  [405, 400],
  /**@type {[number, number]}
   // @ts-expect-error */
  BlackFairy_44:        [690, 417],
  /**@type {[number, number]}
   // @ts-expect-error */
  NineTailFox_54:       [690, 460]
};

exports.basePositions = {

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_0_0:             [290, 280],

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_max_max:         [845, 695],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtn:               [320, 675],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtnJump:           [60,  12],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgrade:       [315, 570],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgradeJump:   [28,  28],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanel:        [360, 608],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanelJump:    [100, 20],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkill:             [323, 428],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillJump:         [100, 35],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStance:       [402, 423],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStanceJump:   [100, 35],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStage:              [320, 480],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStageJump:          [38,  38],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeon:                [320, 585],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeonJump:            [37,  47],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipment:         [325, 450],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipmentJump:     [43,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtn:           [325, 420],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtnJump:       [35,  15],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtn:          [315, 422],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtnJump:      [31,  8],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItem:            [320, 480],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItemJump:        [31,  31],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItem:     [320, 620],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItemJump: [26,  26],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBoss:          [320, 439],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBossJump:      [35,  35],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlot:        [730, 644],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlotJump:    [21,  21],

  /**@type {[number, number]}
   // @ts-expect-error */
  mob:                    [581, 350],

  /**@type {[number, number]}
   // @ts-expect-error */
  mobJump:                [28,  22],
};

/**@type {Record<number, [number, number][]>} */
exports.capturePositions = {
};
