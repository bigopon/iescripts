const robot = require('robotjs');

robot.setMouseDelay(2);

const { x: originalX, y: originalY } = robot.getMousePos();

const DEFAULT_CLICK_COUNT = 1000;
const DEFAULT_INTERVAL = 100 * 3; // 300ms
const interval = parseInt(process.argv.slice(2)[0], 10) * 1000 /**API in second */ || DEFAULT_INTERVAL;
const clickCount = parseInt(process.argv.slice(2)[1], 10) || DEFAULT_CLICK_COUNT;

async function run48() {
  robot.moveMouse(originalX, originalY);
  robot.setMouseDelay(0);
  robot.mouseClick();
  robot.setKeyboardDelay(0);
}

robot.mouseClick();
robot.setMouseDelay(0);
run48();
setInterval(() => run48(), 1000 * 17.5);
