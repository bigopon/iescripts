// @ts-check
const robot = require('robotjs');
const { enterArea, waitForTime, generateNitro, useDropBoost, useSlimeCoinBoost } = require("./utils");

let runNumber = 0;
const MAX_48_STAY_LENGTH = 1000 * 17;

async function farm48() {
  runNumber++;
  let start = Date.now();

  await enterArea(48);
  await enterArea(48);

  if (runNumber % 4 === 1) {
    await generateNitro();
  }
  if (runNumber % 15 === 1) {
    await useDropBoost();
    await useSlimeCoinBoost();
  }
  robot.keyTap('u');

  while ((Date.now() - start) < MAX_48_STAY_LENGTH) {
    await waitForTime(1000);
  }
  return farm48();
}

function start() {
  farm48();
}

start();
