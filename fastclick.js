const robot = require('robotjs');

robot.setMouseDelay(2);

const isDoingMission = process.argv.indexOf('--mission') !== -1;
const interval = isDoingMission
  ? 156
  : (parseInt(process.argv.slice(2)[0], 10) || 116);
const { x: originalX, y: originalY } = robot.getMousePos();

let lastClick;
const run = () => {
  const { x: currX, y: currY } = robot.getMousePos();
  // console.log(`Checking position: x:${currX}, y: currY`);
  if ((currX < originalX - 80 || currX > originalX + 80) || (currY < originalY - 80 || currY > originalY + 80)) {
    return process.exit(0);
  }
  // console.log('Clicking...', 'lastClick:', lastClick);
  // lastClick = new Date().toTimeString();
  robot.setMouseDelay(0);
  robot.mouseClick('left');
  robot.setMouseDelay(0);
}

console.log('Clicking every: ' + (interval / 1000) + 'seconds (' + interval + ' ms)');

run();
(async () => {
  while (true) {
    run();
    await new Promise(r => setTimeout(r, interval));
  }
})();
