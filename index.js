// Move the mouse across the screen as a sine wave.
var robot = require("robotjs");
 
// Speed up the mouse.
robot.setMouseDelay(2);
 
const originalMousePos = robot.getMousePos();

// var twoPI = Math.PI * 2.0;
var screenSize = robot.getScreenSize();
// var height = (screenSize.height / 2) - 10;
// var width = screenSize.width;
 
// for (var x = 0; x < width; x++) {
//     y = height * Math.sin((twoPI * x) / width) + height;
//     robot.moveMouse(x, y);
// }

let shouldClick = true;
let lastModifiedShouldClick = Date.now();

function checkShouldClick() {
  const mousePos = robot.getMousePos();
  const isNearEdge = mousePos.x <= screenSize.width * 0.1 || mousePos.x >= screenSize.width * 0.9;
  if (isNearEdge) {
    const now = Date.now();
    if (now - lastModifiedShouldClick > 2000) {
      lastModifiedShouldClick = now;
      shouldClick = !shouldClick;
    }
  }
}

function click() {
  checkShouldClick();
  if (shouldClick) {
    robot.mouseClick('left');
  }
}

// start program
checkShouldClick();
setInterval(click, 50);
