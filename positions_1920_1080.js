// @ts-check

exports.positions = {
  /**@type {[number, number]}
   // @ts-expect-error */
  Upgrade_Slime_Bank:   [610, 930],
  /**@type {[number, number]}
   // @ts-expect-error */
  CraftPanel:           [446, 990],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Items_Tab:      [430, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_Tab:    [520, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Alchemy_UseAll: [701, 864],
  /**@type {[number, number]}
   // @ts-expect-error */
  Craft_Inventory_Tab:  [630, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_D:              [609, 608],
  // when there's no A class item
  // Items_D:              [634, 606],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_C:              [635, 608],
  // when there's no A class item
  // Items_C:              [661, 606],
  /**@type {[number, number]}
   // @ts-expect-error */
  Items_B:              [651, 608],
  // when there's no A class item
  // Items_B:              [688, 606],
  Items_A:              [677, 608],
  /**@type {[number, number]}
   // @ts-expect-error */
  Nitro_Shortcut:       [882, 882],
  /**@type {[number, number]}
   // @ts-expect-error */
  Auto_Move:            [824, 887],
  /**@type {[number, number]}
   // @ts-expect-error */
  Start_Challenge:      [495, 925],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_War:          [510, 605],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Wiz:          [510, 630],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Ang:          [510, 660],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Btn:          [530, 945],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rebirth_Confirm_Btn:  [805, 875],
  /**@type {[number, number]}
   // @ts-expect-error */
  Rein_To_Rebirth_Btn:  [565, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  BlackFairy_44:        [975, 575],
  /**@type {[number, number]}
   // @ts-expect-error */
  NineTailFox_54:       [975, 655],
};

exports.basePositions = {

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_0_0:             [400, 400],

  /**@type {[number, number]}
   // @ts-expect-error */
  canvas_max_max:         [1200, 1000],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtn:               [470, 970],

  /**@type {[number, number]}
   // @ts-expect-error */
  panelBtnJump:           [85,  20],

  /**@type {[number, number]}
   // @ts-expect-error */
  resourceUpgrade:        [435, 775],

  /**@type {[number, number]}
    // @ts-expect-error */
  resourceUpgradeJump:    [40,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgrade:       [435, 820],

  /**@type {[number, number]}
   // @ts-expect-error */
  slimeBankUpgradeJump:   [40,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanel:        [500, 875],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillPanelJump:    [150, 30],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkill:             [450, 615],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillJump:         [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillLevelup:      [563, 628],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillLevelupJump:  [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStance:       [560, 612],

  /**@type {[number, number]}
   // @ts-expect-error */
  classSkillStanceJump:   [150, 50],

  /**@type {[number, number]}
   // @ts-expect-error */
  skillSet:               [807, 918],

  /**@type {[number, number]}
    // @ts-expect-error */
  skillSetJump:           [0,   10],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStage:              [445, 695],

  /**@type {[number, number]}
   // @ts-expect-error */
  areaStageJump:          [55,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeon:                [445, 845],

  /**@type {[number, number]}
   // @ts-expect-error */
  dungeonJump:            [50,  60],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipment:         [455, 650],

  /**@type {[number, number]}
   // @ts-expect-error */
  craftEquipmentJump:     [60,  60],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtn:           [445, 605],

  /**@type {[number, number]}
   // @ts-expect-error */
  enterAreaBtnJump:       [55,  20],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtn:          [435, 605],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyTabBtnJump:      [50,  10],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItem:            [435, 695],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyItemJump:        [45,  40],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItem:     [445, 895],

  /**@type {[number, number]}
   // @ts-expect-error */
  alchemyCraftedItemJump: [37,  37],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBoss:          [445, 625],

  /**@type {[number, number]}
   // @ts-expect-error */
  challengeBossJump:      [55,  50],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlot:        [1035, 930],

  /**@type {[number, number]}
   // @ts-expect-error */
  globalSkillSlotJump:    [30,  30],

  /**@type {[number, number]}
   // @ts-expect-error */
  mob:                    [795, 485],

  /**@type {[number, number]}
   // @ts-expect-error */
  mobJump:                [40,  35],
};

/**@type {Record<number, [number, number][]>} */
exports.capturePositions = {
  61: [
    [884, 587],
    // [923, 587],
    [971, 587],
    // [1017, 587],
    [1064, 587]
  ],
};
