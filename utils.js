// @ts-check
const robot = require('robotjs');
const {
  positions,
  delays,
  getCraftEquipmentPosition,
  getPanelBtnPosition,
  getChallengePosition,
  getAlchemyTabBtnPosition,
  getAlchemyItemPosition,
  getClassSkillPanelPosition,
  getClassSkillStancePosition,
  getDungeonPosition,
  getEnterAreaBtnPosition,
  getAreaStagePosition,
  getSkillSetBtnPosition,
  getSystemConfigCheckboxPosition,
  getAlchemyStatBtnPosition,
  getOneTimeQuestRewardClaimBtnPosition,
  getDailyQuestRewardClaimBtnPosition,
  getRebirthUpgradePosition,
} = require('./positions');
const { getColorValue, getPositionColorValue } = require('./color');
const { classNumberMap } = require('./util_constants');

robot.setMouseDelay(0);
robot.setKeyboardDelay(0);

exports.turnOnNitro = turnOnNitro;
exports.ensureManualMove = ensureManualMove;
exports.ensureAutoMove = ensureAutoMove;
exports.disableSkillAnime = disableSkillAnime;
exports.disableLootLog = disableLootLog;
exports.disableTooltip = disableTooltip;
// panel 1
exports.goPanel = goPanel;
exports.goSlimeBank = goSlimeBank;

// panel 2
exports.useSkillStance = useSkillStance;
exports.useSkillSet = useSkillSet;

// panel 3
exports.enterDungeon = enterDungeon;
exports.isAreaAvailable = isAreaAvailable;
exports.isAreaStageAvailable = isAreaStageAvailable;
exports.ensureAreaAvailable = ensureAreaAvailable;
exports.enterArea = enterArea;

// panel 4
exports.isChallengeAvailable = isChallengeAvailable;
exports.ensureChallengeAvailable = ensureChallengeAvailable;
exports.isChallengeNumberAvailable = isChallengeNumberAvailable;
exports.goChallenge = enterChallenge;

// panel 5 -- Craft
exports.goEquipments = goEquipments;
exports.evolveEquipments = evolveEquipments;
exports.equipItems = equipItems;
exports.doEquipItems = doEquipItems;

exports.goAlchemy = goAlchemy;
exports.goInventory = goInventory;
exports.tintureChallengeBoss = tintureChallengeBoss;
exports.isAlchemyTabAvailable = isAlchemyTabAvailable;
exports.unlockAlchemyItems = unlockAlchemyItems;
exports.craftAlchemyItem = craftAlchemyItem;
exports.levelupAlchemyStat = levelupAlchemyStat;
exports.useDropBoost = useDropBoost;
exports.useSlimeCoinBoost = useSlimeCoinBoost;
exports.generateNitro = generateNitro;

// panel 6 -- Quest
exports.canClaimQuest = canClaimQuestAt;
exports.claimAllQuestRewards = claimAllQuestRewards;

// panel 7 -- Rebirth/Reinc
exports.ensureRebirthAvailable = ensureRebirthAvailable;
exports.rebirthAs = rebirthAs;
exports.isRebirthUpgradeAvailable = isRebirthUpgradeAvailable;
exports.isReincAvailable = isReincAvailable;
exports.goReinc = goReinc;
exports.reincAs = reincAs;

// panel 8 -- System
exports.ensureSystemConfigState = ensureSystemConfigState;

// utilities
exports.waitForTime = waitForTime;
exports.ensureClick = ensureClick;
exports.ensureRightClick = ensureRightClick;

const panels = {
  Upgrades: 'upgrades',
  Skills: 'skills',
  Explore: 'explores',
  Challenge: 'Challenge',
  Craft: 'Craft',
  Quest: 'quest',
  Rebirth: 'rebirth',
  System: 'system'
}

async function ensureManualMove() {
  robot.keyTap('a');
  robot.keyTap('s');
  await waitForTime(delays.KeyNormal);
}

async function ensureAutoMove() {
  robot.keyTap('w');
  await waitForTime(delays.KeyNormal * 2);
  await ensureClick(positions.Auto_Move);
}

async function disableSkillAnime() {
  robot.keyTap('b', 'shift');
  await waitForTime(delays.KeyNormal);
}

async function disableLootLog() {
  robot.keyTap('l', 'shift');
  await waitForTime(delays.KeyNormal);
}

async function disableTooltip() {
  robot.keyTap('t', 'shift');
  await waitForTime(delays.KeyNormal);
}

async function turnOnNitro() {
  await ensureClick(positions.Nitro_Shortcut, delays.ClickNormal * 2, delays.MoveMouse * 3);
}

/**
 * @param {number} panelNumber
 */
async function goPanel(panelNumber) {
  const pos = getPanelBtnPosition(panelNumber);
  robot.moveMouse(...pos);
  await waitForTime(delays.MoveMouse);
  robot.mouseClick();
  await waitForTime(delays.ClickPanel - delays.ClickNormal);
  robot.mouseClick();
  await waitForTime(delays.ClickNormal);
}

async function goSlimeBank() {
  await goPanel(1);
  await ensureClick(positions.Upgrade_Slime_Bank, delays.ClickPanel / 2);
}

/**
 * @param {PlayerClass} klass
 * @param {number} stanceNumber
 */
async function useSkillStance(klass, stanceNumber) {
  await goPanel(2);
  await ensureClick(getClassSkillPanelPosition(classNumberMap[klass]), delays.ClickNormal * 3);
  await ensureClick(getClassSkillStancePosition(stanceNumber))
}

/**
 * @param {number} skillSetNumber
 */
async function useSkillSet(skillSetNumber) {
  await ensureClick(getSkillSetBtnPosition(skillSetNumber));
  await ensureClick(getSkillSetBtnPosition(skillSetNumber));
}

/**
 * @param {number} [alchemyItemGroupNumber] the number of the alchemy items group. from 1 -> 14
 */
async function goAlchemy(alchemyItemGroupNumber) {
  const craftPanelNumber = isChallengeAvailable() ? 5 : 4;
  await goPanel(craftPanelNumber);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  if (alchemyItemGroupNumber) {
    await ensureClick(getAlchemyTabBtnPosition(alchemyItemGroupNumber));
  }
}

async function goInventory() {
  const craftPanelNumber = isChallengeAvailable() ? 5 : 4;
  await goPanel(craftPanelNumber);
  await ensureClick(positions.Craft_Inventory_Tab);
  await ensureClick(positions.Craft_Inventory_Tab);
}

/**
 * @param {number} areaNumber 1-8: the number of the area to enter (there are 8 areas, 1: slime, 2: bat... etc)
 */
function isAreaAvailable(areaNumber) {
  const areaBtnPos = getEnterAreaBtnPosition(areaNumber);
  const isAvailable = getColorValue(robot.getPixelColor(...areaBtnPos)) > getColorValue('000000');
  return isAvailable;
}

/**
 * @param {number} stageNumber 1-8: the number of the stage to enter (1 area has 8 stages)
 */
function isAreaStageAvailable(stageNumber) {
  const [x, y] = getAreaStagePosition(stageNumber);
  const isAvailable =
    getColorValue(robot.getPixelColor(
      // middle of area btn is mob image, in black
      // not working for 1, because of the number shape
      // but 1 can be detected by area vailable so it's ok
      x - 20,
      y - 22
    ))
    > getColorValue('000000');
  return isAvailable;
}

/**
 * Unlike other ensure utilities, this utility needs to use mouse to verify if a stage is available.
 * Needs to be used carefully.
 *
 * @param {number} areaNumber 1-8: the number of the area to enter (there are 8 areas, 1: slime, 2: bat... etc)
 * @param {number} [stageNumber] 1-8: the number of the stage to enter (1 area has 8 stages)
 * @param {(isReady: boolean, runNumber: number) => Promise<number>} [activity] guaranteed to be run at least once, even the area is available straighaway
 * @param {number} [interval] interval to call the activity
 */
async function ensureAreaAvailable(areaNumber, stageNumber, activity, interval = 1000 * 30) {
  const CHECK_INTERVAL = 1000 * 4;
  // input is in 2 digit (48/56/15) format
  // use first digit for area number, 2nd for stage number
  if (stageNumber == null && areaNumber > 10) {
    stageNumber = areaNumber % 10;
    areaNumber = Math.floor(areaNumber / 10);
  }
  // accepting flexible input = need validation
  // no area 8 availabe, only compare against 7 max
  if (areaNumber < 1 || areaNumber > 7) {
    throw new Error('Invalid area: ' + areaNumber);
  }
  if (stageNumber != null && (stageNumber < 1 || stageNumber > 8)) {
    throw new Error('Invalid stage number: ' + stageNumber)
  }
  await new Promise(async r => {
    let activityRunNumber = 0;
    let lastActivityStart = Date.now();
    while (true) {
      let ready = false;
      await goPanel(3);
      if (isAreaAvailable(areaNumber)) {
        if (stageNumber == null) {
          ready = true;
        } else {
          await ensureClick(getEnterAreaBtnPosition(areaNumber));
          if (isAreaStageAvailable(stageNumber)) {
            ready = true;
          }
        }
      }
      if (typeof activity === 'function') {
        if (activityRunNumber === 0 || (Date.now() - lastActivityStart > interval)) {
          activityRunNumber++;
          const beforeActivity = Date.now();
          const activityResult = await activity(ready, activityRunNumber);
          const afterActivity = Date.now();
          lastActivityStart = beforeActivity;
          const shouldWaitForInterval = activityResult === 0;
          if (shouldWaitForInterval) {
            await waitForTime(Math.max(interval - (afterActivity - beforeActivity), 0));
          }
        }
      }

      if (ready) {
        break;
      } else {
        if (typeof activity !== 'function') {
          await waitForTime(CHECK_INTERVAL);
        }
      }
    }
    r();
  //   const intervalId = setInterval(async () => {
  //     await goPanel(3);
  //     if (!isAreaAvailable(areaNumber)) {
  //       return;
  //     }
  //     if (stageNumber != null) {
  //       await ensureClick(getEnterAreaBtnPosition(areaNumber));
  //       if (!isAreaStageAvailable(stageNumber)) {
  //         return;
  //       }
  //     }
  //     clearInterval(intervalId);
  //     r();
  //   }, CHECK_INTERVAL);
  });
}

/**
 * @param {number} areaNumber 1-8: the number of the area to enter (there are 8 areas, 1: slime, 2: bat... etc)
 * @param {number} [stageNumber] 1-8: the number of the stage to enter (1 area has 8 stages)
 */
async function enterArea(areaNumber, stageNumber) {
  // input is in 2 digit (48/56/15) format
  // use first digit for area number, 2nd for stage number
  if (stageNumber == null) {
    stageNumber = areaNumber % 10;
    areaNumber = Math.floor(areaNumber / 10);
  }
  // accepting flexible input = need validation
  // no area 8 availabe, only compare against 7 max
  if (areaNumber < 1 || areaNumber > 7) {
    throw new Error('Invalid area: ' + areaNumber);
  }
  if (stageNumber < 1 || stageNumber > 8) {
    throw new Error('Invalid stage number: ' + stageNumber)
  }
  await goPanel(3);
  await ensureClick(getEnterAreaBtnPosition(areaNumber), delays.ClickPanel / 1.5);
  await ensureClick(getAreaStagePosition(stageNumber), delays.ClickNormal * 2);
}

/**
 * Enter a dungeon based on its position. 1->8
 * @param {number} dungeonNumber
 */
async function enterDungeon(dungeonNumber) {
  await goPanel(3);
  await ensureClick(getDungeonPosition(dungeonNumber), delays.ClickNormal * 2);
}

/**
 * @param {number} challengeNumber
 * @param {number} count
 */
async function tintureChallengeBoss(challengeNumber, count = 1) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab);
  await ensureClick(getAlchemyTabBtnPosition(/* 10L */5));
  const tintureItemPos = getAlchemyItemPosition(/* tinture starts at 7 */6 + challengeNumber);
  while (count-- > 0) {
    await ensureClick(tintureItemPos);
  }
  await ensureClick(positions.Craft_Alchemy_UseAll);
}

function isChallengeAvailable() {
  const panel_7_pos = getPanelBtnPosition(7);
  const isAvailable = robot.getPixelColor(...panel_7_pos) !== '001800';
  return isAvailable;
}

async function ensureChallengeAvailable() {
  await new Promise(r => {
    const startTime = Date.now();
    let intervalId = setInterval(() => {
      if (isChallengeAvailable()) {
        console.log('Challenge available after:', ((Date.now() - startTime) / 1000).toFixed(3), 'seconds');
        clearInterval(intervalId);
        r();
        return;
      }
    }, 1000 * 7);
  });
}

/**
 * @param {number} challengeNumber
 */
function isChallengeNumberAvailable(challengeNumber) {
  const pos = getChallengePosition(challengeNumber);
  return getColorValue(robot.getPixelColor(pos[0] - 18, pos[1] - 20)) > getColorValue('000000');
}

/**
 * @param {number} challengeNumber
 */
async function enterChallenge(challengeNumber) {
  await goPanel(4);
  const pos = getChallengePosition(challengeNumber);
  await ensureClick(pos, delays.ClickPanel, delays.MoveMouse * 2);
  await ensureClick(positions.Start_Challenge, undefined, delays.MoveMouse * 2);
  await ensureClick(positions.Start_Challenge);
}

/**
 * @param {number} tabNumber
 */
async function isAlchemyTabAvailable(tabNumber) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  const color = robot.getPixelColor(...getAlchemyTabBtnPosition(tabNumber));
  // available = not too dark (gray'd out)
  return (parseInt(color.slice(0, 2)) || 0) > 65;
}

/**
 * @param {number} tabNumber
 * @param {number[]} itemNumbers
 */
async function unlockAlchemyItems(tabNumber, itemNumbers) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  await ensureClick(getAlchemyTabBtnPosition(tabNumber), delays.ClickPanel / 2);
  await ensureClick(getAlchemyTabBtnPosition(tabNumber), delays.ClickPanel / 2);
  for (const itemNumber of itemNumbers) {
    await ensureClick(getAlchemyItemPosition(itemNumber), delays.ClickNormal * 2);
  }
}

/**
 * @param {number} tabNumber
 * @param {number} itemNumber
 */
async function craftAlchemyItem(tabNumber, itemNumber, itemCount = 1) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  await ensureClick(getAlchemyTabBtnPosition(tabNumber), delays.ClickPanel / 2);
  await ensureClick(getAlchemyTabBtnPosition(tabNumber), delays.ClickPanel / 2);
  while (itemCount--) {
    await ensureClick(getAlchemyItemPosition(itemNumber));
  }
}

/**
 * @param {number} statNumber 1-4. 1: HP/MP, 2: ATK + DEF, 3: MATK + MDEF, 4: SPD
 * @param {number} time time to press mouse for leveling up (30s ~ 300 lvs)
 */
async function levelupAlchemyStat(statNumber, time) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  robot.moveMouse(...getAlchemyStatBtnPosition(statNumber));
  return new Promise(r => {
    robot.mouseToggle('down');
    setTimeout(() => {
      robot.mouseToggle('up');
      r();
    }, time);
  });
}

async function useDropBoost() {
  const tabNumber = /* 10L */5;
  const canCraft = await isAlchemyTabAvailable(tabNumber);
  if (!canCraft) {
    return;
  }
  await craftAlchemyItem(tabNumber, /* drop boost */6);
  robot.keyTap('u');
}

async function useSlimeCoinBoost() {
  const tabNumber = /* 1KL */7;
  const canCraft = await isAlchemyTabAvailable(tabNumber);
  if (!canCraft) {
    return;
  }
  await craftAlchemyItem(tabNumber, /* sc boost */5);
  robot.keyTap('u');
}

async function goEquipments() {
  const craftPanelNumber = isChallengeAvailable() ? 5 : 4;
  // const panelPosition = getPanelBtnPosition(craftPanelNumber);
  await goPanel(craftPanelNumber);
  await ensureClick(positions.Craft_Items_Tab, delays.ClickPanel / 1.5);
}

/**
 * When start, the craft panel is at position number 4
 * @param {ItemClass} quality
 * @param {number[]} itemNumbers
 */
async function equipItems(quality, ...itemNumbers) {
  await goEquipments();
  // const craftPanelNumber = isChallengeAvailable() ? 5 : 4;
  // const panelPosition = getPanelBtnPosition(craftPanelNumber);
  // await goPanel(craftPanelNumber);
  // await ensureClick(positions.Craft_Items_Tab, delays.ClickPanel / 1.5);
  // robot.moveMouse(...panelPosition);
  // await waitForTime(delays.MoveMouse);
  // robot.mouseClick();
  // await waitForTime(delays.ClickPanel);
  // robot.moveMouse(...positions.Craft_Items_Tab);
  // await waitForTime(delays.MoveMouse);
  // robot.mouseClick();
  // await waitForTime(delays.ClickPanel / 1.5);
  await doEquipItems(quality, ...itemNumbers);
}

/**
 * @param {ItemClass} quality
 * @param  {number[]} itemNumbers
 */
async function doEquipItems(quality, ...itemNumbers) {
  /**@type {[number, number]} */
  let itemGroupPos = [0, 0];
  let itemPositions = itemNumbers.map(number => getCraftEquipmentPosition(number));
  switch (quality.toUpperCase()) {
    case 'D':
      itemGroupPos = positions.Items_D;
      break;
    case 'C':
      itemGroupPos = positions.Items_C;
      break;
    case 'B':
      itemGroupPos = positions.Items_B;
      break;
    case 'A':
      itemGroupPos = positions.Items_A;
      break;
    default:
      throw new Error('Invalid item group');
  }
  robot.moveMouse(...itemGroupPos);
  await waitForTime(delays.MoveMouse);
  robot.mouseClick();
  await waitForTime(delays.ClickPanel / 2);
  for (const itemPosition of itemPositions) {
    await ensureClick(itemPosition, delays.ClickNormal * 2);
    await ensureClick(itemPosition, delays.ClickNormal);
  }
}


/**
 * @param {Record<number, number>} evolutions
 */
async function evolveEquipments(evolutions) {
  await goEquipments();
  await doEvolveItems(evolutions);
}

/**
 * @param {Record<number, number>} evolutions
 */
async function doEvolveItems(evolutions) {
  for (const $itemNumber in evolutions) {
    const itemNumber = parseInt($itemNumber, 10);
    const itemPosition = getCraftEquipmentPosition(itemNumber);
    robot.moveMouse(...itemPosition);
    let evolutionCount = evolutions[itemNumber];
    while (evolutionCount--) {
      robot.keyTap('m');
      robot.keyTap('e');
      robot.keyTap('m');
    }
  }
}

async function generateNitro(count = 1) {
  const early = !isChallengeAvailable();
  await goPanel(early ? 4 : 5);
  await ensureClick(positions.Craft_Alchemy_Tab, delays.ClickPanel / 2);
  while (count-- > 0) {
    await go1mL_genNitro();
    if (count > 0) {
      await waitForTime(1000 * 1.5);
    }
  }
}

async function go1mL_genNitro() {
  // go 1mL
  robot.moveMouse(...getAlchemyTabBtnPosition(1));
  await waitForTime(delays.MoveMouse);
  robot.mouseClick();
  await waitForTime(delays.ClickPanel);
  // gen nitro
  robot.moveMouse(...getAlchemyItemPosition(/* 1mL nitro pos */5));
  await waitForTime(delays.MoveMouse);
  robot.keyTap('m');
  robot.keyTap('u');
  await waitForTime(delays.KeyNormal);
}
//#region quest panel
/**
 * @param {[number, number]} questClaimBtnPosition
 */
function canClaimQuestAt(questClaimBtnPosition) {
  return getColorValue(robot.getPixelColor(...questClaimBtnPosition)) > getColorValue('555555');
}

async function claimAllQuestRewards() {
  const early = !isChallengeAvailable();
  await goPanel(early ? 5 : 6);
  let questNumber = 1;
  while (questNumber < 10) {
    const questClaimBtnPos = getOneTimeQuestRewardClaimBtnPosition(questNumber);
    while (canClaimQuestAt(questClaimBtnPos)) {
      await ensureClick(questClaimBtnPos);
      await waitForTime(116);
    }
    questNumber++;
  }
  questNumber = 1;
  while (questNumber < 5) {
    const questClaimBtnPos = getDailyQuestRewardClaimBtnPosition(questNumber);
    while (canClaimQuestAt(questClaimBtnPos)) {
      await ensureClick(questClaimBtnPos);
      await waitForTime(116);
    }
    questNumber++;
  }
}
// //#endregion

function isRebirthAvailable() {
  // as long as there's a button at panel no 6
  // rebirth is available
  const panel_6_pos = getPanelBtnPosition(6);
  const isAvailable = robot.getPixelColor(...panel_6_pos) !== '001800';
  return isAvailable;
}

async function ensureRebirthAvailable() {
  await new Promise(r => {
    const startTime = Date.now();
    let intervalId = setInterval(() => {
      if (isRebirthAvailable()) {
        console.log('Rebirth available after:', ((Date.now() - startTime) / 1000).toFixed(3), 'seconds');
        clearInterval(intervalId);
        r();
        return;
      }
    }, 1000 * 7);
  });
}

/**
 * @param {number} upgradeNumber the number of the upgrade. From 1 -> 18
 */
function isRebirthUpgradeAvailable(upgradeNumber) {
  const pos = getRebirthUpgradePosition(upgradeNumber);
  return getPositionColorValue([pos[0] - 15, pos[1] - 15]) > getColorValue('333333');
}

/**
 * @param {PlayerClass} char
 */
async function rebirthAs(char) {
  await goPanel(isChallengeAvailable() ? 7 : 6);
  await waitForTime(120);
  await ensureClick(positions.Rein_To_Rebirth_Btn, delays.ClickPanel);
  await ensureClick(positions.Rein_To_Rebirth_Btn, delays.ClickPanel);

  /**@type {[number, number]} */
  let charBtnPos = [0, 0];
  switch (char) {
    case 'War':
      charBtnPos = positions.Rebirth_War;
      break;
    case 'Wiz':
      charBtnPos = positions.Rebirth_Wiz;
      break;
    case 'Ang':
      charBtnPos = positions.Rebirth_Ang;
      break;
    default:
      throw new Error('Invalid character: ' + char);
  }
  // select the char to rebirth
  await ensureClick(charBtnPos, delays.ClickNormal * 2);
  await ensureClick(charBtnPos, delays.ClickNormal * 2);

  // rebirth
  await ensureClick(positions.Rebirth_Btn, delays.ClickNormal * 2);
  await ensureClick(positions.Rebirth_Btn, delays.ClickNormal * 2);

  // confirm
  await ensureClick(positions.Rebirth_Confirm_Btn, delays.ClickNormal * 2);
  await ensureClick(positions.Rebirth_Confirm_Btn, delays.ClickNormal * 2);

  // wait for 10s to show start screen
  await waitForTime(1000 * 10);

  // click any where
  await ensureClick(positions.Rebirth_Confirm_Btn);
  await ensureClick(positions.Rebirth_Confirm_Btn);
}

function isReincAvailable() {
  return getColorValue(robot.getPixelColor(positions.Rein_Btn[0] + 40, positions.Rein_Btn[1] - 6))
    > getColorValue('777777');
}

async function goReinc() {
  await goPanel(isChallengeAvailable() ? 7 : 6);
  await ensureClick(positions.Rebirth_To_Rein_Btn, delays.ClickPanel / 2);
  await ensureClick(positions.Rebirth_To_Rein_Btn);
}

/**
 * @param {PlayerClass} char
 */
async function reincAs(char) {
  await goPanel(isChallengeAvailable() ? 7 : 6);
  await ensureClick(positions.Rebirth_To_Rein_Btn, delays.ClickPanel / 2);
  await ensureClick(positions.Rebirth_To_Rein_Btn, delays.ClickPanel / 2);

  await ensureClick(positions.Rein_Btn, delays.ClickNormal * 2);
  await ensureClick(positions.Rein_Btn, delays.ClickNormal * 2);

  await ensureClick(positions.Rein_Confirm_Btn, delays.ClickNormal * 2);
  await ensureClick(positions.Rein_Confirm_Btn, delays.ClickNormal * 2);
  // wait for 10s to show start screen
  await waitForTime(1000 * 7);
  // click any where
  await ensureClick(positions.Rebirth_Confirm_Btn);
  await ensureClick(positions.Rebirth_Confirm_Btn);
  // wait for 10s to show start screen
  await waitForTime(1000 * 7);
  /**@type {[number, number]} */
  let charBtnPos = [0, 0];
  switch (char) {
    case 'War':
      charBtnPos = positions.Class_Selection_War;
      break;
    case 'Wiz':
      charBtnPos = positions.Class_Selection_Wiz;
      break;
    case 'Ang':
      charBtnPos = positions.Class_Selection_Ang;
      break;
    default:
      throw new Error('Invalid character: ' + char);
  }
  await ensureClick(charBtnPos, delays.ClickNormal * 2);
  await ensureClick(charBtnPos, delays.ClickNormal * 2);

  await ensureClick(positions.Class_Selection_Confirm_Btn);
  await ensureClick(positions.Class_Selection_Confirm_Btn);

  await waitForTime(1000 * 7);
}

/**
 * @param {number} configNumber
 */
async function isSystemConfigChecked(configNumber) {
  await goPanel(8);
  const [x, y] = getSystemConfigCheckboxPosition(configNumber);
  // 555555 is a random middle range value that is guaranteed to be brighter than the background
  // when a checkbox is not checked
  return getColorValue(robot.getPixelColor(x, y)) > getColorValue('555555');
}

/**
 * @param {number} configNumber
 * @param {boolean} enabled
 */
async function ensureSystemConfigState(configNumber, enabled) {
  const isChecked = await isSystemConfigChecked(configNumber);
  const pos = getSystemConfigCheckboxPosition(configNumber);
  if (enabled !== isChecked) {
    await ensureClick(pos);
  }
}

/**
 * @param {[number, number]} pos the position to click on
 */
async function ensureClick(pos, clickDelay = delays.ClickNormal, moveDelay = delays.MoveMouse) {
  robot.moveMouse(pos[0], pos[1]);
  robot.moveMouse(pos[0] + 1, pos[1] + 1);
  await waitForTime(moveDelay);
  robot.mouseClick();
  await waitForTime(clickDelay);
}

/**
 * @param {[number, number]} pos the position to click on
 */
async function ensureRightClick(pos, clickDelay = delays.ClickNormal, moveDelay = delays.MoveMouse) {
  robot.moveMouse(pos[0], pos[1]);
  robot.moveMouse(pos[0] + 1, pos[1] + 1);
  await waitForTime(moveDelay);
  robot.mouseClick('right');
  await waitForTime(clickDelay);
}

/**
 * @param {number} ms
 */
async function waitForTime(ms) {
  await new Promise(r => setTimeout(r, ms));
}
