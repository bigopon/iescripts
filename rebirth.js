// @ts-check
/// <reference path="./types.d.ts" />

const robot = require('robotjs');
const {
  equipItems,
  disableSkillAnime,
  turnOnNitro,
  ensureAutoMove,
  ensureChallengeAvailable,
  ensureManualMove,
  goChallenge,
  waitForTime,
  generateNitro,
  rebirthAs,
  disableLootLog,
  disableTooltip,
  useSkillStance,
  goPanel,
  goSlimeBank,
  ensureRightClick
} = require("./utils");
const { delays, getSlimeBankUpgradeItemPosition } = require('./positions');

/**@type {PlayerClass[]}
 // @ts-expect-error */
const runs = Array(20).fill('Wiz,Ang,War').join(',').split(',');
const char = process.argv.slice(2)[0];

/**
 * @type {PlayerClass}
 */
let currentRun;
let rbRunNumber = 0;
/**@type {Date} */
let currentRunStart;

function nextRun() {
  const run = runs.shift();
  runs.push(run);
  return run;
}

function nextRandomRun() {
  const index = Math.floor(Math.random() * runs.length);
  const run = runs[index];
  runs.splice(index, 1);
  runs.push(run);

  return run;
}

async function equipAllItems() {
  await equipItems('D', 5, 6, 7, 8, 9, 10);
  await equipItems('C', 1, /* 2, */ 3, 5, 7);
  await equipItems('B', 1, 2, 3, 4, 6, /* 7->10 all optional, prefer speed from fairy set */ 9, 10, 7, 8,);
  await equipItems('A', 1, 2, 3, 5);
}

async function increaseGamePerf() {
  await disableSkillAnime();
  await disableLootLog();
  await disableTooltip();
}

async function moveToCorner() {
  robot.keyTap('s');
  robot.keyTap('a');
  robot.keyTap('a');
  // ang too fast to move 3 times
  // move too far unnecessarily
  if (currentRun !== 'Ang') {
    robot.keyTap('a');
  }
  await waitForTime(delays.KeyNormal);
}

async function enterChallenge() {
  await ensureChallengeAvailable();
  await ensureManualMove();
  await goChallenge(1);
  // await moveToCorner();
}

function ensureEnoughNitro() {
  const intervalId = setInterval(
    () => generateNitro(),
    1000 * 85
  );
  return {
    dispose: () => {
      clearInterval(intervalId);
    }
  };
}

async function startNewRun() {
  currentRun = nextRun();
  while (char && currentRun !== char) {
    currentRun = nextRun();
  }
  currentRunStart = new Date();
  console.log('Starting new run with:', currentRun, currentRunStart.toJSON());
  // start again
  await ensureAutoMove();
  await rebirthAs(currentRun);
  await waitForTime(6000);
  return startRebirthRun();
}

async function startRebirthRun() {
  rbRunNumber++;
  console.log('Running rb run No.', rbRunNumber);
  await increaseGamePerf();
  await turnOnNitro();
  await generateNitro(1);

  await equipAllItems();

  await generateNitro(1);

  await enterChallenge();
  console.log('Enter challenge at:', new Date().toJSON());
  await generateNitro();
  const { dispose } = ensureEnoughNitro();
  // after 2 minutes, end the rebirth run
  // by this time, it should already be about 15k RP
  // todo: read the level/RP gain via OCR tool and auto tweak this number
  await waitForTime(1000 * 60 * 0.5);
  dispose();
  console.log('Rebirthing...', 'after:', ((Date.now() - currentRunStart.getTime()) / 1000).toFixed(3), 'seconds');
  return startNewRun();
}

// startRebirthRun();

startNewRun();
