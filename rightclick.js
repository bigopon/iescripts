const robot = require('robotjs');

robot.setMouseDelay(0);

const { x: originalX, y: originalY } = robot.getMousePos();

const DEFAULT_INTERVAL = 1000 * 0.155; // 3 mins
const interval = (parseFloat(process.argv.slice(2)[0], 10) /**API in second */) || DEFAULT_INTERVAL;

const run = () => {
  const { x: currX, y: currY } = robot.getMousePos();
  console.log(`Checking position: x:${currX}, y: currY`);
  if ((currX < originalX - 80 || currX > originalX + 580) || (currY < originalY - 380 || currY > originalY + 380)) {
    // return process.exit(0);
  } else {
    console.log('Right clicking...');
    robot.mouseClick('right');
  }
}

console.log('Clicking every: ' + (interval / 1000) + 'seconds (' + interval + ' ms)');

// run();
(async () => {
  let i = 10000;
  while (i--) {
    run();

    await new Promise(r => setTimeout(r, interval));
  }
})();
