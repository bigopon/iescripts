const robot = require('robotjs');

robot.setMouseDelay(2);

const { x: originalX, y: originalY } = robot.getMousePos();

const DEFAULT_CLICK_COUNT = 1000;
const DEFAULT_INTERVAL = 100 * 3; // 300ms
const interval = parseInt(process.argv.slice(2)[0], 10) * 1000 /**API in second */ || DEFAULT_INTERVAL;
const clickCount = parseInt(process.argv.slice(2)[1], 10) || DEFAULT_CLICK_COUNT;

let lastClick;
const run = () => {
  const { x: currX, y: currY } = robot.getMousePos();
  console.log(`Checking position: x:${currX}, y: currY`);
  if (currX !== originalX || currY !== originalY) {
    return process.exit(0);
  }
  console.log('Clicking...', 'lastClick:', lastClick);
  lastClick = new Date().toTimeString();
  robot.setMouseDelay(0);
  robot.mouseClick('left', true);
  robot.setMouseDelay(0);
}

console.log('Clicking every: ' + (interval / 1000) + 'seconds (' + interval + ' ms)');

run();
(async () => {
  let i = clickCount;
  while (i > 0) {
    run();
    i--;
    await new Promise(r => setTimeout(r, interval));
  }
})();
