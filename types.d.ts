type PlayerClass = 'War' | 'Wiz' | 'Ang' | 'War_R' | 'Wiz_R' | 'Ang_R';

type ItemClass = 'D' | 'C' | 'B' | 'A';

interface ReinRun {
  startStep: number;
  char: PlayerClass;
  start: Date;
  done: boolean;
  log: (msg: string) => void;
  logStep: (step: ReinRunStepInfo) => void;
}

interface ReinRunStepInfo {
  number: typeof ReinRunStep[keyof typeof ReinRunStep];
  start: Date;
  duration: number;
}

const ReinRunStep = {
  Starting: 1,
  Leveling_Skill: 2,
  Leveling_AlchemyStats: 3,
  Unlocking_SkillSlot_n_Resources3: 4,
  Unlocking_Area3: 5,
  Unlocking_Area4: 6,
  Unlocking_Resources4: 7,
  Unlocking_Area5: 8,
  Unlocking_Monty: 9,
  Unlocking_Challenge: 10,
  Preparing_Monthy: 11,
  Trying_Monty: 12,
  Finishing: 13,
} as const
