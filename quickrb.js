// @ts-check
/// <reference path="./types.d.ts" />

const {
  equipItems,
  disableSkillAnime,
  turnOnNitro,
  ensureAutoMove,
  waitForTime,
  generateNitro,
  rebirthAs,
  disableLootLog,
  disableTooltip,
  ensureRebirthAvailable
} = require("./utils");

/**@type {PlayerClass[]}
 // @ts-expect-error */
const runs = Array(20).fill('Wiz,Ang,War').join(',').split(',');
const chars = process.argv.slice(2)[3];

/**
 * @type {PlayerClass}
 */
let currentRun;
let rbRunNumber = 0;
/**@type {Date} */
let currentRunStart;

function nextRun() {
  const run = runs.shift();
  runs.push(run);
  return run;
}

function nextRandomRun() {
  const index = Math.floor(Math.random() * runs.length);
  const run = runs[index];
  runs.splice(index, 1);
  runs.push(run);

  return run;
}

async function equipAllItems() {
  await equipItems('D', 5, 6, 7, 8, 9, 10);
  await equipItems('C', 1, /* 2, */ 3, 5, 7);
  await equipItems('B', 1, 2, 3, 4, 6, /* 7->10 all optional, prefer speed from fairy set */ 9, 10, 7, 8,);
}

async function increaseGamePerf() {
  await disableSkillAnime();
  await disableLootLog();
  await disableTooltip();
}

async function startNewRun() {
  currentRun = nextRun();
  let selectCharAttempt = 0;
  while (chars.length === 0 || chars.indexOf(currentRun) !== -1) {
    currentRun = nextRun();
    selectCharAttempt++;
    if (selectCharAttempt >= 5) {
      // throw new Error('Cannot select character');
      currentRun = 'Wiz'; // fastest
    }
  }
  currentRunStart = new Date();
  console.log('Starting new run with:', currentRun, currentRunStart.toJSON());
  // start again
  await ensureAutoMove();
  await rebirthAs(currentRun);
  await waitForTime(6000);
  return startRebirthRun();
}

async function startRebirthRun() {
  rbRunNumber++;
  console.log('Running rb run No.', rbRunNumber);
  await increaseGamePerf();
  await turnOnNitro();
  await generateNitro(1);

  await equipAllItems();

  await ensureRebirthAvailable();
  console.log('Rebirthing...', 'after:', ((Date.now() - currentRunStart.getTime()) / 1000).toFixed(3), 'seconds');
  return startNewRun();
}

startNewRun();
