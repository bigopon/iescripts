// @ts-check
/// <reference path="./types.d.ts" />

const path = require('path');
const fs = require('fs');
const robot = require('robotjs');
const {
  ensureSystemConfigState,
  generateNitro,
  goPanel,
  disableSkillAnime,
  disableLootLog,
  disableTooltip,
  turnOnNitro,
  waitForTime,
  unlockAlchemyItems,
  ensureClick,
  reincAs,
  levelupAlchemyStat,
  ensureRebirthAvailable,
  rebirthAs,
  ensureAreaAvailable,
  enterArea,
  claimAllQuestRewards,
  goEquipments,
  useSkillStance,
  evolveEquipments,
  ensureChallengeAvailable,
  isReincAvailable,
  goReinc,
  isAreaStageAvailable,
  isAreaAvailable,
  isChallengeNumberAvailable,
  ensureRightClick,
  goInventory,
  craftAlchemyItem,
  goAlchemy,
  goChallenge,
  isRebirthUpgradeAvailable
} = require('./utils');
const {
  getClassSkillLevelupPosition,
  getResourceUpgradeItemPosition,
  getClassSkillPanelPosition,
  getClassSkillPosition,
  getRebirthUpgradePosition,
  delays,
  basePositions,
  getCraftEquipmentPosition,
  positions,
  getChallengePosition,
  getEnterAreaBtnPosition,
  getInventoryItemSellMaxBtnPosition,
  getAlchemyItemPosition,
} = require('./positions');
const { classNumberMap } = require('./util_constants');
const { getColorValue, getPositionColorValue } = require('./color');

/**
 * @type {{ [T in PlayerClass]: Record<number, number> }}
 // @ts-expect-error*/
const classResourceUpgradesEarly = {
  War: {},
  // early game upgrade position are different to their normal
  Wiz: {
    2: 2,
    3: 2,
    18: 2,
    19: 2,
    10: 20,
    11: Number.MAX_SAFE_INTEGER,
    13: Number.MAX_SAFE_INTEGER,
    25: Number.MAX_SAFE_INTEGER,
    // 29: Infinity,
    // 30: Infinity,
  },
  Ang: {}
};

/**
 * @type {{ [T in PlayerClass]: Record<number, number> }}
 // @ts-expect-error*/
const classResourceUpgradesAfter23 = {
  War: {},
  // early game upgrade position are different to their normal
  Wiz: {
    4: 2,
    5: 2,
    20: 2,
    21: 2,
    13: Number.MAX_SAFE_INTEGER,
    29: Number.MAX_SAFE_INTEGER,
    30: Number.MAX_SAFE_INTEGER,
  },
  Ang: {}
};

/**
 * @type {{ [T in PlayerClass]: Record<number, number> }}
 // @ts-expect-error*/
 const classResourceUpgrades_After35 = {
  War: {},
  // early game upgrade position are different to their normal
  Wiz: {
    6: 2,
    7: 2,
    22: 2,
    23: 2,
    // remove from super queue
    13: Number.MIN_SAFE_INTEGER,
  },
  Ang: {}
};

/**
 * @type {{ [T in PlayerClass]: Record<number, number> }}
 // @ts-expect-error*/
 const classResourceUpgrades_After44 = {
  War: {},
  // early game upgrade position are different to their normal
  Wiz: {
    6: 2,
    7: 2,
    22: 2,
    23: 2,
    13: Number.MAX_SAFE_INTEGER,
    15: Number.MAX_SAFE_INTEGER,
  },
  Ang: {}
};

const [MIN_X, MIN_Y] = basePositions.canvas_0_0;
const [MAX_X, MAX_Y] = basePositions.canvas_max_max;
/**
 * @param {number} x
 * @param {number} y
 */
function isInThreshold(x, y) {
  return x >= MIN_X && x <= MAX_X && y >= MIN_Y && y <= MAX_Y;
}

function checkStop() {
  const pos = robot.getMousePos();
  let shouldContinue = isInThreshold(pos.x, pos.y);
  if (!shouldContinue) {
    console.log('Invalid mouse position', pos.x, pos.y, '\n>>> exiting...');
    process.exit(0);
  }
}

/**
 * @param {PlayerClass} playerClass 
 */
async function clickResources(playerClass, count = 1) {
  /**@type {[number, number]} */
  let res_pos = [0, 0];
  switch (playerClass) {
    case 'War': res_pos = positions.Click_Stone;
      break;
    case 'Wiz': res_pos = positions.Click_Crystal;
      break;
    case 'Ang': res_pos = positions.Click_Leaf;
      break;
    default: throw new Error('Invalid player class');
  }
  while (count > 0) {
    await ensureClick(res_pos);
    --count;
  }
}

/**
 * @param {PlayerClass} playerClass
 * @param {number} batchCount
 */
async function craftResourceBoost(playerClass, batchCount = 1) {
  if (playerClass === 'Wiz') {
    await goAlchemy(1);
    robot.moveMouse(...getAlchemyItemPosition(3));
    while (batchCount-- > 0) {
      robot.keyTap('m');
      robot.keyTap('u');
      if (batchCount > 0) {
        await waitForTime(1000 * 0.3);
      }
    }
  }
}

async function craftAllResourcesBoost(count = 1) {
  await goAlchemy(6);
  robot.moveMouse(...getAlchemyItemPosition(4));
  while (count-- > 0) {
    robot.keyTap('m');
    robot.keyTap('u');
    if (count > 0) {
      await waitForTime(1000 * 0.3);
    }
  }
}

async function boostAlchemySpeed(count = 1, phase = 1) {
  await goAlchemy(phase);
  robot.moveMouse(...getAlchemyItemPosition(1));
  while (count-- > 0) {
    robot.keyTap('m');
    robot.keyTap('u');
    if (count > 0) {
      await waitForTime(1000 * 0.3);
    }
  }
}

async function unlockCoreAlchemyItems() {
  // resources && nitro
  await unlockAlchemyItems(1, [2, 3, 4, 5]);
  // resources jar
  await unlockAlchemyItems(6, [4]);
  // capsules
  // await unlockAlchemyItems(2, [10, 11, 12]);
}

/**
 * @param {PlayerClass} playerKlass
 */
async function queueUpgrades_Early(playerKlass) {
  await goPanel(1);
  // find the right upgrade for the class
  // also queue gold upgrade
  const upgradesToQueue = classResourceUpgradesEarly[playerKlass];
  if (!upgradesToQueue) {
    throw new Error('Invalid class');
  }
  await doQueueUpgrades(upgradesToQueue);
}

/**
 * @param {PlayerClass} playerKlass
 */
async function queueUpgrades_After23(playerKlass) {
  await goPanel(1);
  // find the right upgrade for the class
  // also queue gold upgrade
  const upgradesToQueue = classResourceUpgradesAfter23[playerKlass];
  if (!upgradesToQueue) {
    throw new Error('Invalid class');
  }
  await doQueueUpgrades(upgradesToQueue);
}

/**
 * @param {PlayerClass} playerKlass
 */
async function queueUpgrades_After35(playerKlass) {
  await goPanel(1);
  // find the right upgrade for the class
  // also queue gold upgrade
  const upgradesToQueue = classResourceUpgrades_After35[playerKlass];
  if (!upgradesToQueue) {
    throw new Error('Invalid class');
  }
  await doQueueUpgrades(upgradesToQueue);
}

/**
 * @param {PlayerClass} playerKlass
 */
async function queueUpgrades_After44(playerKlass) {
  await goPanel(1);
  // find the right upgrade for the class
  // also queue gold upgrade
  const upgradesToQueue = classResourceUpgrades_After44[playerKlass];
  if (!upgradesToQueue) {
    throw new Error('Invalid class');
  }
  await doQueueUpgrades(upgradesToQueue);
}

/**
 * @param {Record<number, number>} upgrades
 */
async function doQueueUpgrades(upgrades) {
  for (const $upgradeNumber in upgrades) {
    const upgradeNumber = parseInt($upgradeNumber, 10);
    const pos = getResourceUpgradeItemPosition(upgradeNumber);
    robot.moveMouse(pos[0], pos[1]);
    let upgradeCount = upgrades[upgradeNumber];
    if (Number.MAX_SAFE_INTEGER === upgradeCount) {
      robot.keyTap('q');
    } else if (Number.MIN_SAFE_INTEGER === upgradeCount) {
      robot.keyTap('q', 'control');
    } else {
      while (upgradeCount--) {
        robot.mouseClick('right');
        await waitForTime(delays.ClickNormal);
      }
    }
  }
}

/**
 * @param {Record<number, number>} upgrades
 */
async function doUpgrade(upgrades) {
  for (const $upgradeNumber in upgrades) {
    const upgradeNumber = parseInt($upgradeNumber, 10);
    const pos = getResourceUpgradeItemPosition(upgradeNumber);
    robot.moveMouse(pos[0], pos[1]);
    let upgradeCount = upgrades[upgradeNumber];
    while (upgradeCount--) {
      robot.mouseClick();
      await waitForTime(delays.ClickNormal);
    }
  }
}

/**
 * @param {Record<number, number>} upgrades
 */
async function upgradeResources(upgrades) {
  await goPanel(1);
  await doUpgrade(upgrades);
}

/**
 * @param {PlayerClass} playerClass
 * @param {number[]} skillNumbers
 */
async function levelupSkills(playerClass, skillNumbers, tryCount = 1, countInterval = 1000 * 30) {
  await goPanel(2);
  await ensureClick(getClassSkillPanelPosition(classNumberMap[playerClass]));
  while (tryCount--) {
    // just constantly level up everything, maybe
    for (const i of skillNumbers) {
      const pos = getClassSkillLevelupPosition(i);
      robot.moveMouse(pos[0], pos[1]);
      robot.keyTap('m');
    }
    if (tryCount > 0) {
      await waitForTime(countInterval);
    }
  }
}

/**
 * @param {PlayerClass} playerClass
 */
async function ensureSkillStanceOn(playerClass) {
  if (playerClass === 'Wiz') {
    await useSkillStance(playerClass, 1);
    await useSkillStance(playerClass, 2);
  }
}


async function ensureUnlockAndEquipItems(maxLevel = false) {
  // keep clicking on all items and maxing them all
  // q: how to know when to evolve?
  await goEquipments();
  await ensureClick(/* first item group from the right */[677, 608]);
  await ensureClick(/* first item group from the right */[677, 608]);
  for (let itemNumber = 0; 10 > itemNumber; ++itemNumber) {
    await ensureClick(getCraftEquipmentPosition(itemNumber));
    await ensureClick(getCraftEquipmentPosition(itemNumber));
    if (maxLevel) {
      robot.keyTap('m');
    }
  }
}

async function increaseGamePerf() {
  await disableSkillAnime();
  await disableLootLog();
  await disableTooltip();
}

async function ensureProperSystemConfig() {
  // auto progress
  await ensureAutoProgress();
  await ensureAutoProgress();
  await ensureSystemConfigState(/* auto cast active */13, true);
  await ensureSystemConfigState(/* auto cast active */13, true);
}

async function ensureAutoProgress() {
  await ensureSystemConfigState(/* auto progress */4, true);
}

async function ensureNoAutoProgress() {
  await ensureSystemConfigState(/* auto progress */4, false);
}

async function ensureWizSkills(phase = 1) {
  await goPanel(2);
  await waitForTime(250);

  await ensureClick(getClassSkillPanelPosition(/* wiz */3));
  await ensureClick(getClassSkillPanelPosition(/* wiz */3));

  // equip fireball
  await ensureClick(getClassSkillPosition(2));
  robot.keyTap('2');

  // equip firestorm
  await ensureClick(getClassSkillPosition(3));
  robot.keyTap('1', 'shift');

  if (phase < 1) {
    return;
  }

  await ensureClick(getClassSkillPanelPosition(/* wiz_r */4));
  await ensureClick(getClassSkillPanelPosition(/* wiz_r */4));

  // equip combo
  await ensureClick(getClassSkillPosition(2));
  robot.keyTap('1');

  await ensureClick(getClassSkillPanelPosition(/* wiz */3));
  await ensureClick(getClassSkillPanelPosition(/* wiz */3));

  // equip meteor
  await ensureClick(getClassSkillPosition(4));
  robot.keyTap('3');

  // equip iceball
  await ensureClick(getClassSkillPosition(5));
  robot.keyTap('2', 'shift');

  // equip thunderball
  await ensureClick(getClassSkillPosition(7));
  robot.keyTap('3', 'shift');

  if (phase < 2) {
    return;
  }

  await ensureClick(getClassSkillPanelPosition(/* wiz_r */4));
  await ensureClick(getClassSkillPanelPosition(/* wiz_r */4));

  // equip manashield
  await ensureClick(getClassSkillPosition(3));
  robot.keyTap('3');

  await ensureClick(getClassSkillPanelPosition(/* wiz */3));
  await ensureClick(getClassSkillPanelPosition(/* wiz */3));

  // equip meteor
  await ensureClick(getClassSkillPosition(4));
  robot.keyTap('1', 'shift');

  // equip blizard
  await ensureClick(getClassSkillPosition(7));
  robot.keyTap('3', 'shift');

  // equip lightingthunder
  await ensureClick(getClassSkillPosition(10));
  robot.keyTap('2', 'shift');

  // monty phase
  if (phase < 4) {
    return;
  }
  // replace manashield with firestorm
  // so it can get monty faster
  await ensureClick(getClassSkillPosition(3));
  robot.keyTap('3');
}

/**
 * @param {PlayerClass} char
 */
async function ensureSkillsEquipped(char, phase = 1) {
  // todo: parameter based
  await ensureWizSkills(phase);
}

/**
 * Some classes like Wizard has passive that reduces cd of skills
 * though it doesn't always work immediately. Refreshing stance seems to fix it
 * @param {PlayerClass} char
 */
async function refreshSkills(char) {
  if (char === 'Wiz') {
    await goPanel(2);
    await ensureClick(getClassSkillPanelPosition(classNumberMap[char]));
    await ensureClick(getClassSkillPanelPosition(classNumberMap[char]));
    robot.keyTap('t', 'shift');
    // passive bar
    robot.moveMouse(570, 850);
    await waitForTime(1000 * 3);
    robot.moveMouse(575, 850);
    await waitForTime(1000 * 1);
    // maybe 10s is enough to refresh
    // await waitForTime(1000 * 5);
    // robot.moveMouse(580, 850);
    // await waitForTime(delays.MoveMouse);
    robot.keyTap('t', 'shift');
  }
}

async function spendWizRP() {
  await goPanel(6);
  await waitForTime(150);
  const upgrades = [
    [/* global slot */6, 1],
    [/* g cap */ 7, 1],
    [/* r rate */8, 20],
    [/* mp */    9, 10],
    [/* matk */  10, 10],
    [/* mdef */  11, 10],
    [/* equip */ 12, 1],
  ];
  const theRest = [
    [8, 65],
    [10, 30],
    [6, 1],
    [7, 10],
    [12, 1],
  ];
  for (let [upgradeNumber, upgradeCount] of upgrades) {
    const pos = getRebirthUpgradePosition(upgradeNumber);
    while (upgradeCount--) {
      await ensureClick(pos);
    }
  }
  for (let [upgradeNumber, upgradeCount] of theRest) {
    const pos = getRebirthUpgradePosition(upgradeNumber);
    while (isRebirthUpgradeAvailable(upgradeNumber) && upgradeCount--) {
      await ensureClick(pos, delays.ClickNormal / 3);
    }
  }
}

/**
 * @param {PlayerClass} char
 */
async function spendRebirthPoints(char) {
  if (char === 'Wiz') {
    await spendWizRP();
  }
}

/**
 * @param {PlayerClass} char
 * @param {number} area
 * @param {number} time
 * @param {(activityRun: number) => any} [extraActivity]
 * @param {number} interval
 */
async function farmGold(char, area, time, extraActivity, interval = 1000 * 30) {
  await enterArea(area);
  await enterArea(area);
  await enterArea(area);

  await ensureNoAutoProgress();
  await ensureNoAutoProgress();

  if (typeof extraActivity === 'function') {
    await new Promise(async r => {
      let $time = time;
      let activityRun = 0;
      while ($time > 0) {
        activityRun++;
        const beforeActivity = Date.now();
        await extraActivity(activityRun);
        const afterActivity = Date.now();
        $time -= interval;
        await waitForTime(Math.max(interval - (afterActivity - beforeActivity), 0));
      }
      r();
    });
  } else {
    await waitForTime(time);
  }

  await ensureAutoProgress();
  await ensureAutoProgress();
  await ensureAutoProgress();
}

async function trySellMax(tryCount = 100, startAt = 1) {
  await goInventory();
  let i = 0;
  while (i < tryCount) {
    await ensureClick(getInventoryItemSellMaxBtnPosition(startAt));
    i++;
  }
}

async function getLastChallengeNumber() {
  await goPanel(4);
  for (let challengeNumber = 7; challengeNumber > 1; --challengeNumber) {
    if (isChallengeNumberAvailable(challengeNumber)) {
      return challengeNumber;
    }
  }
  return 1;
}

async function tryMonty() {
  const lastChallengeNumber = await getLastChallengeNumber();
  await new Promise(async r => {
    let tryCount = 0;
    while (true) {
      tryCount++;
      await goPanel(4);
      // need to focus on monty
      if (tryCount === 1) {
        await ensureClick(getChallengePosition(lastChallengeNumber));
        await ensureClick(getChallengePosition(lastChallengeNumber));
      }

      /**@type {[number, number]} */
      const bossHpBarPos = [1070, 412];
      // while (getPositionColorValue(bossHpBarPos) > getColorValue('111111') === false
      //   && getPositionColorValue(positions.Quit_Challenge) > getColorValue('777777')
      // ) {
      //   checkStop();
      //   await ensureClick(positions.Quit_Challenge, 0);
      // }

      const bigSlimePos = [972, 511];
      while ((getColorValue(robot.getPixelColor(bigSlimePos[0], bigSlimePos[1])) > getColorValue('333333')) === false) {
        // checkStop();
        // if (getPositionColorValue(bossHpBarPos) > getColorValue('111111')
        //   || getPositionColorValue(positions.Quit_Challenge) > getColorValue('777777')
        // ) {
        //   checkStop();
        //   await ensureClick(positions.Quit_Challenge, 0);
        // }
        // console.log('color is:', robot.getPixelColor(bigSlimePos[0], bigSlimePos[1]));
        await waitForTime(50);
      }
      await waitForTime(120 + Math.round(Math.random() * 30));
      await ensureClick(positions.Start_Challenge);
      await goReinc();
      // checkStop();
      if (isReincAvailable()) {
        break;
      }
    }
    r();
  });
}

/**@type {typeof ReinRunStep} */
const steps = {
  Starting: 1,
  Leveling_Skill: 2,
  Leveling_AlchemyStats: 3,
  Unlocking_SkillSlot_n_Resources3: 4,
  Unlocking_Area3: 5,
  Unlocking_Area4: 6,
  Unlocking_Resources4: 7,
  Unlocking_Area5: 8,
  Unlocking_Monty: 9,
  Unlocking_Challenge: 10,
  Preparing_Monthy: 11,
  Trying_Monty: 12,
  Finishing: 13,
};

/**
 * @param {ReinRun} currReinRun
 * @param {number} reinRunNumber
 */
async function startReincRun(currReinRun, reinRunNumber) {
  let step = 0;
  /**@type {string[]} */
  // const logs = [];
  /**@param {string} msg */
  const logStep = (msg) => {
    const $msg = `    [${reinRunNumber} -- ${currReinRun.char}] <Step ${++step}>: ${msg} @${new Date().toJSON()}`;
    // logs.push($msg);
    currReinRun.log($msg);
  };
  /**
   * @param {typeof ReinRunStep[keyof typeof ReinRunStep]} stepNumber
   * @param {() => any} task
   */
  const withLog = async (stepNumber, task) => {
    const start = new Date();
    await task();
    currReinRun.logStep({ number: stepNumber, start, duration: Date.now() - start.getTime() });
  }
  const currentRun = currReinRun.char;
  const allSkills = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const all_R_skills = [1, 2, 3];
  const startStep = currReinRun.startStep;

  if (startStep <= steps.Starting) {
    // logStep('startup');
    // const start = new Date();
    await withLog(steps.Starting, async () => {
      await clickResources(currentRun, 5);
      await levelupSkills(currentRun, [1, 2]);
      await increaseGamePerf();
      await ensureProperSystemConfig();
      await queueUpgrades_Early(currentRun);
      await ensureSkillsEquipped(currentRun, 0);

      await unlockCoreAlchemyItems();
      // q: how to know when it's sufficient for nitro and nitro has been on?
      await generateNitro();
      await turnOnNitro();

      // ensure resources are ready for another 3s
      await waitForTime(1000 * 3);
      await generateNitro(1);
    });
    // currReinRun.logStep({ number: steps.Starting, start, duration: Date.now() - start.getTime() });
  }

  if (startStep <= steps.Leveling_Skill) {
    // logStep('leveling skills');
    await withLog(steps.Leveling_Skill, async () => {
      // @ts-ignore leveling R skills first as they cost a lot
      await levelupSkills(`${currentRun}_R`, all_R_skills);
      await waitForTime(1000 * 1);
      await ensureSkillsEquipped(currentRun, 0);
      await levelupSkills(currentRun, [3]);
      await waitForTime(1000 * 1);
      await generateNitro(1);

      await levelupSkills(currentRun, [5]);
      await ensureSkillsEquipped(currentRun);
      await waitForTime(1000 * 1);
      await levelupSkills(currentRun, [7]);
      await waitForTime(1000 * 1);
      await ensureSkillsEquipped(currentRun);
      await generateNitro(1);
    });
    // currReinRun.logStep({ number: steps.Leveling_Skill, start, duration: Date.now() - start.getTime() });
  }

  if (startStep <= steps.Leveling_AlchemyStats) {
    // logStep('leveling alchemy stats');
    await withLog(steps.Leveling_AlchemyStats, async () => {
      // levelup atk to help with clear speed
      await levelupAlchemyStat(/* atk + def */2, 1000 * 10);
      await levelupAlchemyStat(/* spd */4, 1000 * 20);
      await craftResourceBoost(currentRun, 5);
      await levelupSkills(currentRun, [1, 2, 3, 4, 5, 6, 7]);
      await waitForTime(1000);
      await ensureSkillsEquipped(currentRun);

      await generateNitro(1);

      // lvup spd to help with clear speed
      await levelupAlchemyStat(/* spd */4, 1000 * 10);

      // 1-4 unlock stat upgrades
      await ensureAreaAvailable(1, 5);
    });
  }

  if (startStep <= steps.Unlocking_SkillSlot_n_Resources3) {
    // logStep('Unlocking 3rd skill slot & resources 3');
    await withLog(steps.Unlocking_SkillSlot_n_Resources3, async () => {
      await enterArea(2, 1);
      await enterArea(2, 1);

      await generateNitro(2);

      await ensureAreaAvailable(2, 2);

      await claimAllQuestRewards();
      await levelupSkills(currentRun, allSkills);
      await ensureSkillsEquipped(currentRun);

      await generateNitro(1);
      await levelupAlchemyStat(/* matk + mdef */3, 1000 * 25);
      await levelupAlchemyStat(/* hp + mp */4, 1000 * 10);

      // make sure resource level 3 available
      await ensureAreaAvailable(2, 4);
    });
  }

  if (startStep <= steps.Unlocking_Area3) {
    // logStep('unlocking area 3');
    await withLog(steps.Unlocking_Area3, async () => {
      await ensureAreaAvailable(3, void 0, async (ready, activityRunNumber) => {
        if (ready) {
          return 1;
        }
        if (activityRunNumber > 7) {
          await waitForTime(1000 * 2.5);
          return 1;
        }
        if (activityRunNumber === 1) {
          await queueUpgrades_After23(currentRun);
          // repeat leveling skill to make sure skills are available
          // @ts-ignore
          await levelupSkills(`${currentRun}_R`, all_R_skills);
          await levelupSkills(currentRun, allSkills);
          await generateNitro(2);
        }

        await craftResourceBoost(currentRun, 5);
        await levelupSkills(currentRun, allSkills);

        // at this point meteor should be ready
        if (activityRunNumber % 3 === 2) {
          await ensureSkillsEquipped(currentRun);
          await refreshSkills(currentRun);
        }

        if (activityRunNumber === 4) {
          // lvup spd to help with clear speed
          await levelupAlchemyStat(/* spd */4, 1000 * 15);
        }

        if (activityRunNumber > 5) {
          await refreshSkills(currentRun);
          await ensureSkillStanceOn(currentRun);
        }
      }, 1000 * 10);
    });
  }

  if (startStep <= steps.Unlocking_Area4) {
    // logStep('unlocking area 4');
    await withLog(steps.Unlocking_Area4, async () => {
      await enterArea(3, 1);
      await enterArea(3, 1);
      await ensureAreaAvailable(4, void 0, async (ready, activityRunNumber) => {
        if (ready) {
          return 1;
        }

        if (activityRunNumber > 7) {
          await waitForTime(1000 * 2.5);
          return 1;
        }

        if (activityRunNumber === 1) {
          await generateNitro(2);
        }

        if (activityRunNumber === 3) {
          // await ensureSkillsEquipped(currentRun, 2);
          await ensureUnlockAndEquipItems(true);
          await boostAlchemySpeed(5, /* boost phase 2 for bigger speed */4);
          await levelupAlchemyStat(/* spd */4, 1000 * 15);
          await generateNitro(2);
        }

        await craftResourceBoost(currentRun, 5);
        await levelupSkills(currentRun, allSkills);
  
        if (activityRunNumber === 5) {
          await levelupAlchemyStat(/* spd */4, 1000 * 15);
        }

        return 0;
      }, 1000 * 15);
    });
  }

  if (startStep <= steps.Unlocking_Resources4) {
    // logStep('clearing area 4s & unlocking 4th resource upgrade');
    await withLog(steps.Unlocking_Resources4, async () => {
      await enterArea(4, 1);
      await enterArea(4, 1);
      await ensureAreaAvailable(4, 5, async (ready, activityRunNumber) => {
        if (ready) {
          return 1;
        }

        if (activityRunNumber > 7) {
          await waitForTime(1000 * 2.5);
          return 1;
        }

        if (activityRunNumber === 1) {
          await generateNitro(3);
          await ensureSkillsEquipped(currentRun, 2);
  
          await evolveEquipments({
            // 4: 5,
            // 6: 1,
            8: 1,
            9: 2,
          });
        }

        if (activityRunNumber % 2 === 1) {
          await levelupSkills(currentRun, allSkills);
          await craftResourceBoost(currentRun, 5);
        }

        return 0;
      }, 1000 * 10);
    });
  }

  if (startStep <= steps.Unlocking_Area5) {
    // logStep('unlocking area 5');
    await withLog(steps.Unlocking_Area5, async () => {
      await ensureAreaAvailable(5, void 0, async (ready, activityRunNumber) => {
        if (ready) {
          return 1;
        }

        if (activityRunNumber > 7) {
          await waitForTime(1000 * 2.5);
          return 1;
        }

        if (activityRunNumber === 1) {
          await queueUpgrades_After44(currentRun);
          await upgradeResources({
            12: 20,
            15: 20
          });
        }

        if (activityRunNumber % 2 === 1) {
          await levelupSkills(currentRun, allSkills);
        }

        await craftResourceBoost(currentRun, 10);

        if (activityRunNumber === 4) {
          await unlockCoreAlchemyItems();
          await craftAllResourcesBoost(2);
          await trySellMax(50);
          // @ts-expect-error
          await levelupSkills(`${currentRun}_R`, all_R_skills);
        }

        if (activityRunNumber === 5) {
          await trySellMax(50, 10);
        }

        return 0;
      }, 1000 * 20);
    });
  }

  if (startStep <= steps.Unlocking_Monty) {
    // logStep('unlocking monty');
    await withLog(steps.Unlocking_Monty, async () => {
      await enterArea(5, 1);
      await enterArea(5, 1);
      await ensureAreaAvailable(5, 5, async (ready, activityRunNumber) => {
        if (ready) {
          return 1;
        }

        if (activityRunNumber > 7) {
          await waitForTime(1000 * 2.5);
          return 1;
        }

        // if (activityRunNumber === 1) {
        //   await ensureSkillsEquipped(currentRun, 3);
        // }

        if (activityRunNumber % 2 === 1) {
          await levelupSkills(currentRun, allSkills);
          await craftResourceBoost(currentRun, 10);
        }

        if (activityRunNumber > 0 && activityRunNumber % 4 === 1) {
          await generateNitro(2);
        }

        return 0;
      }, 1000 * 10);
    });
  }

  if (startStep <= steps.Unlocking_Challenge) {
    // logStep('unlocking challenge');
    await withLog(steps.Unlocking_Challenge, async () => {
      await enterArea(1, 5);
      await enterArea(1, 5);
      await enterArea(1, 5);
      await generateNitro();

      await ensureChallengeAvailable();
    });
  }

  if (startStep <= steps.Preparing_Monthy) {
    // logStep('Preparing for monty');
    await withLog(steps.Preparing_Monthy, async () => {
      await ensureNoAutoProgress();
      await ensureNoAutoProgress();

      await enterArea(1, 1);
      await enterArea(1, 1);
      await enterArea(1, 1);

      await ensureSkillsEquipped(currentRun, 4);
    });
  }

  if (startStep <= steps.Trying_Monty) {
    // logStep('trying monty');
    await withLog(steps.Trying_Monty, async () => {
      await tryMonty();
    });
  }

  if (startStep <= steps.Finishing) {
    // logStep('finished');
    await withLog(steps.Finishing, async () => {
      // just a bit more rewards
      await claimAllQuestRewards();
    });
  }
}

/**
 * @param {string} log
 * @param {string} fileName
 */
function saveLog(log, fileName) {
  fileName = fileName || `run_${new Date().toJSON().replace(/\:/g, '_')}`;

  const dirPath = path.resolve(__dirname, 'run_logs');
  const filePath = path.resolve(dirPath, fileName.endsWith('.json') ? fileName : `${fileName}.txt`);
  return new Promise(async r => {
    if (!fs.existsSync(dirPath)){
      fs.mkdirSync(dirPath);
    }

    fs.appendFile(filePath, log, (err) => {
      if (err) {
        console.log('Save log error');
      } else {
        // console.log(`Saved logs to "run_logs/${filePath}.txt" at ${new Date().toJSON()}`);
      }
      r();
    });
  });
}

const stepMessageMap = {
  1: 'Starting',
  2: 'Leveling basic skills',
  3: 'Leveling alchemy stats',
  4: 'Unlocking 3rd skill slot & resources 3',
  5: 'Unlocking area 3',
  6: 'Unlocking area 4',
  7: 'Unlocking resources 4',
  8: 'Unlocking area 5',
  9: 'Unlocking Monty',
  10: 'Unlocking challenge',
  11: 'Preparing for Monty',
  12: 'Trying Monty',
  13: 'Finishing'
};
const longestMessageLength = Math.max(...Object.values(stepMessageMap).map(msg => msg.length));

/**
 * @param {ReinRunStepInfo[]} stepLogs
 */
function generateTable(stepLogs) {
  // const processedLogs = stepLogs.map((stepLog, idx) => [
  //   /* number */stepLog.number.toString(),
  //   /* start */stepLog.start.toJSON(),
  //   /* duration */(stepLog.duration / 1000).toFixed(3),
  //   /* messages */stepMessageMap[stepLog.number]
  // ]);
  const headerRow = [
    'Step No.'.padEnd(10, ' '),
    'Start'.padEnd(25, ' '),
    'Duration'.padEnd(20, ' '),
    'Description'.padEnd(longestMessageLength + 3, ' ')
  ];
  return [
    headerRow.join('|  '),
    ...stepLogs.map((stepLog, idx) => [
      /* number */stepLog.number.toString().padEnd(10, ' '),
      /* start */stepLog.start.toJSON().padEnd(25, ' '),
      /* duration */`${(stepLog.duration / 1000).toFixed(3)} seconds`.padEnd(20, ' '),
      /* messages */stepMessageMap[stepLog.number].padEnd(longestMessageLength + 3, ' ')
    ].join('|  ')),
    ''
    // ...processedLogs.map(processedLog => {
    //   const [id, start, duration, msg] = processedLog;
    //   return [
    //     id.padEnd(headerRow[0].length, ' '),
    //     start.padEnd(25, ' '),
    //     + `${duration} seconds`.padEnd(20, ' ')
    //     + msg.padEnd(longestMessageLength + 3, ' ')
    // })
  ].join('\n');
}

/**
 * @param {ReinRun} reinRun
 * @param {ReinRunStepInfo[]} stepLogs
 * @param {string} runName
 * @param {number} totalDuration
 * @param {number} reincRunNumber
 */
function saveStepLogs(reinRun, stepLogs, runName, totalDuration, reincRunNumber) {
  const logs = [
    `Starting new run with: ${JSON.stringify(reinRun)}`,
    `>   Run No. ${reincRunNumber}`,
    `>   Total duration: ${(totalDuration / 1000).toFixed(3)} seconds`,
    'Steps:',
    generateTable(stepLogs),
    '-----------------------------',
    '\n',
  ];
  const log = logs.join('\n');
  console.log(log);
  return Promise.all([
    saveLog(log, runName),
    saveLog(
      JSON.stringify({
        ...reinRun,
        totalDuration,
        runName,
        reincRunNumber,
        steps: stepLogs
      }),
      `${runName}.json`
    )
  ]);
}

async function start() {
  const runName = (() => {
    let runNameIndex = process.argv.indexOf('--name');
    return (process.argv[runNameIndex + 1] || `reinc_run_${new Date().toJSON()}`)
      .replace(/\:/g, '_');
  })();
  const startStep = (() => {
    let startStepIndex = process.argv.indexOf('--start');
    return parseInt(process.argv[startStepIndex + 1]) || 0;
  })();

  let reinRunNumber = 0;
  while (true) {
    reinRunNumber++;
    /**@type {string[]} */
    let logs = [];
    /**@type {ReinRunStepInfo[]} */
    let stepLogs = [];
    /**@type {ReinRun} */
    const currentRun = {
      startStep: reinRunNumber === 1 ? startStep : 0,
      char: 'Wiz',
      start: new Date(),
      done: false,
      /**@param {string} msg */
      log: (msg) => {
        logs.push(msg);
        console.log(msg);
      },
      logStep: (step) => {
        stepLogs.push(step);
      }
    };
    // currentRun.log(`Starting new run with: ${JSON.stringify(currentRun)}`);
    // currentRun.log(`>   Run No. ${reinRunNumber}`);

    const shouldReinFirst = currentRun.startStep === 0;
    if (shouldReinFirst) {
      await reincAs(currentRun.char);
    }
    await Promise.race([
      startReincRun(currentRun, reinRunNumber)
        .then(($logs) => {
          currentRun.done = true;
          // fire and forget
          // saveLog(logs, runName).catch();
        }),
      // start another "thread" to check support force exit
      new Promise(async r => {
        while (!currentRun.done) {
          checkStop();
          await waitForTime(500);
        }
        r();
      })
    ]);
    // currentRun.log(`Reincanating... after: ${((Date.now() - currentRun.start.getTime()) / 1000).toFixed(3)} seconds`);
    // currentRun.log('-----------------------------');
    // currentRun.log('\n');
    // saveLog(logs.join('\n'), runName).catch();
    saveStepLogs(
      currentRun,
      stepLogs,
      runName,
      /* total duration */Date.now() - currentRun.start.getTime(),
      /* relative run number */reinRunNumber
    ).catch();
  }
}

start();
// tryMonty();
