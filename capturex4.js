// @ts-check
const robot = require('robotjs');
const {
  goAlchemy,
  waitForTime,
  ensureRightClick,
  generateNitro,
  useDropBoost,
  goPanel,
  ensureClick,
  ensureAutoMove,
  useSkillSet,
  enterArea
} = require('./utils');
const {
  getAlchemyItemPosition,
  delays,
  getAlchemyCraftedItemPosition,
  getEnterAreaBtnPosition,
  getAreaStagePosition,
  getClassSkillPosition,
  getClassSkillPanelPosition,
  getGlobalSkillSlotPosition,
  basePositions,
  positions
} = require('./positions');

const [MIN_X, MIN_Y] = basePositions.canvas_0_0;
const [MAX_X, MAX_Y] = basePositions.canvas_max_max;

const areaStageNumber = parseInt(process.argv.slice(2)[0], 10);
const isDoingMission = process.argv.indexOf('--mission') !== -1;

/**
 * @param {number} x
 * @param {number} y
 */
function isInThreshold(x, y) {
  return x >= MIN_X && x <= MAX_X && y >= MIN_Y && y <= MAX_Y;
}

function getAreaNumber() {
  return Math.floor(areaStageNumber / 10) || 4;
}

function checkContinue() {
  const pos = robot.getMousePos();
  let shouldContinue = isInThreshold(pos.x, pos.y);
  if (!shouldContinue) {
    console.log('Invalid mouse position', pos.x, pos.y, '\n>>> exiting...');
  }
  return shouldContinue;
}

function hasNet() {
  const pos = getAlchemyCraftedItemPosition(1);
  const color = robot.getPixelColor(...pos);
  return color !== '1e1e1e';
}

async function generateNet() {
  await goAlchemy(/* 1L */4);
  robot.moveMouse(...getAlchemyItemPosition(14));
  await waitForTime(delays.MoveMouse);
  robot.keyTap('m');
  await waitForTime(delays.MoveMouse);
}

/**
 * @param {number} areaNumber
 * @param {[number, number]} position
 */
async function capture(areaNumber, position) {
  await ensureEmptySkills();
  await generateNet();
  await goPanel(3);
  await ensureClick(getEnterAreaBtnPosition(areaNumber));
  for (const i of Array(12)) {
    if (!checkContinue()) {
      process.exit(0);
      break;
    }
    await ensureClick(getAreaStagePosition(4), 0);
    robot.keyTap('s');
    robot.keyTap('s');
    robot.keyTap('s');
    await ensureRightClick(position, 0);
    if (isDoingMission) {
      // refreshing too fast makes the count not have enough time to register
      await waitForTime(455);
    }
    if (!checkContinue()) {
      process.exit(0);
      break;
    }
  }
  // exit 4-4 for safety
  await ensureClick(getAreaStagePosition(2));
}

async function ensureEmptySkills() {
  await useSkillSet(2);
}

async function ensureSkills() {
  await useSkillSet(1);
}

async function useFanSwing() {
  await goPanel(2);
  await ensureClick(getClassSkillPanelPosition(1));
  await ensureClick(getClassSkillPosition(8));
  robot.keyTap('1', 'shift');
}

async function useBlizzard() {
  await goPanel(2);
  await ensureClick(getClassSkillPanelPosition(3));
  await ensureClick(getClassSkillPosition(7));
  robot.keyTap('2', 'shift');
}

async function farmGold(time = 7) {
  // then go 3-3 to farm net
  // await goPanel(3);
  // await ensureClick(getEnterAreaBtnPosition(3));
  // await ensureClick(getAreaStagePosition(3));
  await enterArea(3, 3);
  await enterArea(3, 3);
  // ensure things are smooth
  await ensureAutoMove();
  // await useFanSwing();
  // await useBlizzard();
  await ensureSkills();
  // farm a bit for gold
  await waitForTime(1000 * time);
}

/**
 * @returns {[number, number]}
 */
function getBossPosition() {
  switch (areaStageNumber) {
    case 54: return positions.NineTailFox_54;
    case 44:
    default:
      return positions.BlackFairy_44;
  }
}

async function start() {
  let taskNumber = 0;
  const baseTasks = [
    async () => {
      taskNumber++;
      await farmGold(5);
      for (let i = 0; i < 4; ++i) {
        await capture(getAreaNumber(), getBossPosition());
      }
    },
    async () => {
      if (taskNumber % 2 === 1) {
        await generateNitro();
      }
      if (taskNumber % 4 === 1) {
        await useDropBoost();
        await generateNitro();
      }
      // just to be sure
      robot.keyTap('u');
      await waitForTime(delays.KeyNormal);
    }
  ];
  while (baseTasks.length > 0) {
    const task = baseTasks.shift();
    baseTasks.push(task);
    await task();
  }
}

start();
