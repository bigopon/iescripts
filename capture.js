// @ts-check
/// <reference path="./types.d.ts" />

const robot = require('robotjs');
const {
  goAlchemy,
  waitForTime,
  ensureRightClick,
  generateNitro,
  useDropBoost,
  enterArea,
  ensureAutoMove,
  enterDungeon,
  useSlimeCoinBoost
} = require("./utils");
const {
  basePositions,
  delays,
  getAlchemyItemPosition,
  getMobPosition,
  getAlchemyCraftedItemPosition,
  capturePositions,
} = require('./positions');

const noEnterArea = process.argv.slice(2).indexOf('--no-enter') !== -1;

const [MIN_X, MIN_Y] = basePositions.canvas_0_0;
const [MAX_X, MAX_Y] = basePositions.canvas_max_max;

const netFarmingTimer = 1000 * 60 * 7;
const coinFarmingTimer = 1000 * 60 * 8;
const capturingTimer = 1000 * 60 * 25;

const mobStartIndexPositions = {
  // less than 10 means dungeon
  1: 23,
  3: 23,
  4: 23,
  5: 23,
  6: 23,
  12: 23,
  13: 13,
  15: 23,
  16: 22,
  22: 13,
  23: 14,
  26: 11,
  33: 23,
  36: 34,
  37: 33,
  42: 24,
  43: 24,
  46: 13,
  47: 23,
  48: 12,
  53: 5,
  57: 4,
  71: 5,
  72: 24,
  73: 24,
};

const attemptsPerLoop = {
  1: 7,
  4: 7,
  3: 7,
  5: 1,
  6: 1,
  12: 6,
  15: 7,
  16: 1,
  23: 6,
  33: 1,
  36: 5,
  37: 3,
  42: 4,
  43: 5,
  47: 6,
  53: 1,
  57: 1,
  71: 1,
  72: 5,
  73: 5,
};

const noOffset = [0, 0];
const mobOffset = {
  13: [0, 10],
  23: [0, 12],
  33: [-3, 3],
  36: [0, 18],
  37: [-2, -12],
  42: [0, -12],
  47: [0, 20],
  51: [0, 7],
  52: [0, 15],
  // 53: [15, 15]
}

const areaStageNumber = parseInt(process.argv.slice(2)[0], 10);

/**
 * @param {number} x
 * @param {number} y
 */
function isInThreshold(x, y) {
  return x >= MIN_X && x <= MAX_X && y >= MIN_Y && y <= MAX_Y;
}

function checkStop() {
  const pos = robot.getMousePos();
  let shouldContinue = isInThreshold(pos.x, pos.y);
  if (!shouldContinue) {
    console.log('Invalid mouse position', pos.x, pos.y, '\n>>> exiting...');
    process.exit(0);
  }
}

function hasNet() {
  const pos = getAlchemyCraftedItemPosition(1);
  const color = robot.getPixelColor(...pos);
  return color !== '1e1e1e';
}

async function generateNet() {
  await goAlchemy(/* 1L */4);
  robot.moveMouse(...getAlchemyItemPosition(14));
  await waitForTime(delays.MoveMouse);
  robot.keyTap('m');
  await waitForTime(delays.MoveMouse);
}

/**
 * @param {[number, number][]} mobPositions
 */
async function capture(mobPositions) {
  checkStop();
  await generateNet();
  let canCountinue = hasNet();
  while (canCountinue) {
    for (const mobPosition of mobPositions) {
      if (canCountinue) {
        await ensureRightClick(mobPosition);
        checkStop();
        canCountinue = hasNet();
      } else {
        return;
      }
    }
  }
}

/**
 * @param {number} [$areaStageNumber]
 * @param {number} [waveNumber]
 * @returns {[number, number][]}
 */
function calcMobPositions($areaStageNumber, waveNumber) {
  if (capturePositions[$areaStageNumber]) {
    return capturePositions[$areaStageNumber];
  }
  const startMobPosition = mobStartIndexPositions[$areaStageNumber] || 2;
  const attempts = attemptsPerLoop[$areaStageNumber] || 8;
  const offset = mobOffset[$areaStageNumber] || noOffset;
  return Array.from({ length: attempts }, (_, idx) => {
    const position = getMobPosition(idx + startMobPosition);
    return [
      position[0] + offset[0],
      position[1] + offset[1]
    ];
  });
}

async function start() {
  let taskRunCount = 0;
  /**@type {(() => any)[]} */
  const tasks = [
    // () => generateNitro(),
    // () => useDropBoost(),
  ];

  const isDungeon = areaStageNumber < 10;
  // const $netFarmingTimer

  function queueFarmNet() {
    tasks.push(async () => {
      await enterArea(3, 3);
      await enterArea(3, 3);
      await ensureAutoMove();
      let startFarmingTime = Date.now();
      let secondCounter = 0;
      while (Date.now() - startFarmingTime < netFarmingTimer) {
        secondCounter++;
        if (secondCounter % 85 === 1) {
          await generateNitro();
        }
        // if (secondCounter % 180 === 0) {
        //   await useDropBoost();
        // }
        await waitForTime(1000);
      }
      robot.keyTap('u');
      await enterArea(4, 7);
      await new Promise(async (r) => {
        let shouldLeave = false;
        setTimeout(() => {
          shouldLeave = true;
        }, coinFarmingTimer);
        while (!shouldLeave) {
          await capture(calcMobPositions(areaStageNumber));
        }
        r();
      });
      queueEnterArea();
    });
  }

  function queueEnterArea() {
    let shouldCapture = true;
    tasks.push(async () => {
      let captureRunCount = 0;
      if (taskRunCount > 1 || !noEnterArea) {
        if (areaStageNumber > 10) {
          await enterArea(areaStageNumber);
          await enterArea(areaStageNumber);
        } else {
          await enterDungeon(areaStageNumber);
          await enterDungeon(areaStageNumber);
        }
      }
      while (shouldCapture) {
        // await capture(
        //   mobStartIndexPositions[areaStageNumber] || 2,
        //   attemptsPerLoop[areaStageNumber],
        //   mobOffset[areaStageNumber]
        // );
        await capture(calcMobPositions(areaStageNumber));
        captureRunCount++;
        if (captureRunCount % 5 === 1) {
          await generateNitro();
        }
        if (captureRunCount % 25 === 1) {
          await useDropBoost();
          await useSlimeCoinBoost();
        }
        robot.keyTap('u');
      }
    });
    setTimeout(() => {
      shouldCapture = false;
      queueFarmNet();
    }, capturingTimer);
  }

  queueEnterArea();

  while (true) {
    const task = tasks.length > 0 ? tasks.shift() : () => waitForTime(1000);
    taskRunCount++;
    await task();
  }
}

start();
