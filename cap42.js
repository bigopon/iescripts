const robot = require('robotjs');

robot.setMouseDelay(2);

const { x: originalX, y: originalY } = robot.getMousePos();

const DEFAULT_TOTAL_RUNTIME = 60; // 60 minutes
const DEFAULT_INTERVAL = 1000 * 0.5; // 0.3s
const interval = (parseFloat(process.argv.slice(2)[0], 10) * 1000 /**API in second */) || DEFAULT_INTERVAL;
const runtime = parseFloat(process.argv.slice(2)[1], 10) /* API in minutes */ || DEFAULT_TOTAL_RUNTIME;

const run = async () => {
  const clickPoints = getValidCapturePoints42(originalX, originalY);

  for (const point of clickPoints) {
    if (shouldStop()) {
      process.exit(0);
    }
    robot.moveMouse(point.x, point.y);
    robot.setMouseDelay(0);
    robot.mouseClick('right', true);
    robot.setMouseDelay(0);
    await Promise.resolve();
    // await waitFor(90);
    // robot.mouseClick('right');
    // robot.setMouseDelay(0);
  }
}

console.log('Clicking every: ' + (interval / 1000) + 'seconds (' + interval + ' ms)');

const now = Date.now();
(async () => {
  while (true) {
    await run();

    // await new Promise(r => setTimeout(r, interval));
  }

  process.exit(1);

})();

function shouldStop() {
  // console.log('checking stop...');
  if ((Date.now() - now) > 1000 * 60 * runtime) {
    console.log('Pass valid time:', Date.now() - now);
    return true;
  }
  const { x: currX, y: currY } = robot.getMousePos();
  const deltaX = currX - originalX;
  if (deltaX < 0 || deltaX > 550) {
    console.log('DeltaX invalid', deltaX);
    return true;
  }
  const deltaY = currY - originalY;
  if (deltaY < -350 || deltaY > 0) {
    console.log('Delta y invalid', deltaY);
    return true;
  }
}

function waitFor(time = 0) {
  return new Promise(r => setTimeout(r, time));
}

const capturePoints = {
  1: {},
  2: {},
  3: {
    7: (x, y) => getValidCapturePoints(x, y)
  },
  4: {
    2: (x, y) => getValidCapturePoints42(x, y)
  }
};

/**@param {number} originalX @param {number} originalY */
function getValidCapturePoints(originalX, originalY) {
  /**@type {{ x: number; y: number; }[]} */
  const clickPoints = [
    { x: originalX, y: originalY },
    ...Array
      .from({ length: 2 })
      .reduce((arr, _, yIdx) => arr.concat(
        Array
          .from({ length: 7 })
          .map((_, xIdx) => ({
            x: originalX + 90 + 35 * xIdx,
            y: originalY - 200 + 40 * yIdx
          }))
      ), []),
  ];

  return clickPoints
}

/**@param {number} originalX @param {number} originalY */
function getValidCapturePoints42(originalX, originalY) {
  /**@type {{ x: number; y: number; }[]} */
  const clickPoints = [
    { x: originalX, y: originalY },
    ...Array
      .from({ length: 1 })
      .reduce((arr, _, yIdx) => arr.concat(
        Array
          .from({ length: 4 })
          .map((_, xIdx) => ({
            x: originalX + 90 + 35 * xIdx + 2,
            y: originalY - 200 + 40 * yIdx
          }))
      ), []),
  ];

  return clickPoints
}
