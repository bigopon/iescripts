// @ts-check
/// <reference path="./types.d.ts" />

const { enterDungeon, generateNitro, enterArea, waitForTime } = require("./utils");

const dungeonNumber = parseInt(process.argv.slice(2)[0], 10) || /* fairies */4;

const normalAreaTimer = 1000 * 60 * 20;
const dungeonTimer = 1000 * 60 * 20;
const nitroTimer = 1000 * 85;

async function start() {
  const tasks = [];

  let currentGenerateNitro;
  function queueGenerateNitro() {
    tasks.push(() => currentGenerateNitro = generateNitro());
  }

  function queueEnterArea() {
    tasks.push(() => enterArea(4, 7));
    const nitroId = setInterval(queueGenerateNitro, nitroTimer);
    setTimeout(async () => {
      clearInterval(nitroId);
      await currentGenerateNitro;
      queueEnterDungeon();
    }, normalAreaTimer);
  }

  function queueEnterDungeon() {
    tasks.push(() => enterDungeon(dungeonNumber));
    const nitroId = setInterval(queueGenerateNitro, nitroTimer);
    setTimeout(async () => {
      clearInterval(nitroId);
      await currentGenerateNitro;
      queueEnterArea();
    }, dungeonTimer);
  }

  queueEnterDungeon();

  while (true) {
    const task = tasks.length > 0 ? tasks.shift() : () => waitForTime(1000);
    await task();
  }
}

start();
