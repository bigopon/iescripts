// @ts-check
const { goPanel, ensureClick, generateNitro, waitForTime, useDropBoost } = require("./utils");
const { getAreaStagePosition, getEnterAreaBtnPosition } = require("./positions");

let setTourCompleteStatus;
/**@type {Promise<void>} */
let tourCompleted;

let setBoostingCompleteStatus;
/**@type {Promise<void>} */
let boostCompleted;

async function goStageArea4(stageNumber) {
  await goPanel(3);
  await ensureClick(getEnterAreaBtnPosition(stageNumber));
  await ensureClick(getAreaStagePosition(4));
}

function markRunningTour() {
  tourCompleted = new Promise(r => {
    setTourCompleteStatus = r;
  });
}

function markTourCompleted() {
  setTourCompleteStatus();
}

async function runTour() {
  await boostCompleted;
  markRunningTour();
  let i = 1;
  while (i < 6) {
    await goStageArea4(i);
    ++i;
    await waitForTime(/* time for result panel to complete */1000 * 1.5);
  }
  markTourCompleted();
}

function ensureBoosts() {
  const intervalId = setInterval(
    async () => {
      await tourCompleted;
      markUsingBoosts();
      await generateNitro(),
      await useDropBoost();
      markBoostingCompleted();
    },
    1000 * 85
  );
  return {
    dispose: () => {
      clearInterval(intervalId);
    }
  };
}

function markUsingBoosts() {
  boostCompleted = new Promise(r => {
    setBoostingCompleteStatus = r;
  });
}

function markBoostingCompleted() {
  setBoostingCompleteStatus();
}

async function startTour() {
  runTour();
  const tourIntervalId = setInterval(() => {
    runTour();
  }, 1000 * 60 * 8);

  const { dispose } = ensureBoosts();

  return {
    dispose: () => {
      dispose();
      clearInterval(tourIntervalId);
    }
  }
}

startTour();
